

import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
import matplotlib.ticker as ticker
from matplotlib.patches import Ellipse
import matplotlib.transforms as transforms
from scipy.stats import ttest_1samp, boxcox, boxcox_normplot, probplot, shapiro

import dce_radiomics as dr    

#------------------------------------------------------------------
def feature_color(feature_name:str):
    ''''
    Get color of feature using Damien's color scheme
    '''
    if feature_name.startswith('original_shape'):
        col = 'tab:red'
    elif feature_name.startswith('original_firstorder'):
        col = 'tab:blue'
    elif feature_name.startswith('original_glcm'):
        col = 'tab:purple'
    elif feature_name.startswith('original_glrlm'):
        col = 'tab:orange'
    elif feature_name.startswith('original_glszm'):
        col = 'tab:brown'
    elif feature_name.startswith('original_gldm'):
        col = 'tab:pink'
    elif feature_name.startswith('original_ngtdm'):
        col = 'tab:gray'
    else:
        raise ValueError(f'Feature {feature_name} not recognised')

    return col

#------------------------------------------------------------------
def get_feature_colors(features):
    ''''
    '''
    return [feature_color(f) for f in features]

#------------------------------------------------------------------
def get_feature_labels(features):
    '''
    '''
    shape_count = 0
    firstorder_count = 0
    glcm_count = 0
    glrlm_count = 0
    glszm_count = 0
    gldm_count = 0
    ngtdm_count = 0

    feature_labels = []
    split_points = [0]
    n_features = len(features)

    for i_f, f in enumerate(features):

        if f.startswith('original_shape'):
            shape_count += 1
            feature_labels.append(str(shape_count))

        elif f.startswith('original_firstorder'):
            if not firstorder_count:
                split_points.append(i_f/n_features)

            firstorder_count += 1
            feature_labels.append(str(firstorder_count))

        elif f.startswith('original_glcm'):
            if not glcm_count:
                split_points.append(i_f/n_features)

            glcm_count += 1
            feature_labels.append(str(glcm_count))

        elif f.startswith('original_glrlm'):
            if not glrlm_count:
                split_points.append(i_f/n_features)

            glrlm_count += 1
            feature_labels.append(str(glrlm_count))

        elif f.startswith('original_glszm'):
            if not glszm_count:
                split_points.append(i_f/n_features)

            glszm_count += 1
            feature_labels.append(str(glszm_count))

        elif f.startswith('original_gldm'):
            if not gldm_count:
                split_points.append(i_f/n_features)

            gldm_count += 1
            feature_labels.append(str(gldm_count))

        elif f.startswith('original_ngtdm'):
            if not ngtdm_count:
                split_points.append(i_f/n_features)

            ngtdm_count += 1
            feature_labels.append(str(ngtdm_count))

        else:
            raise ValueError(f'Feature {f} not recognised')

    split_points.append(1.0)
    group_labels = [
        'Shape', 
        'First-order', 
        'GLCM', 
        'GLMRLM', 
        'GLSZM', 
        'GLDM', 
        'NGTDM']

    group_centres = [(f1 + f2)/2 for f1, f2 in 
        zip(split_points[0:-1], split_points[1:])]

    return feature_labels, split_points, group_labels, group_centres

#------------------------------------------------------------------
def style_map_name(map_name):
    if map_name == 'Ktrans':
        style_name = '$K^{\mathrm{trans}}$'
    
    elif map_name == 'IAUC60':
        style_name = '$IAUC_{60}$'

    elif map_name == 'v_e':
        style_name = '$v_e$'

    elif map_name == 'v_p':
        style_name = '$v_p$'

    return style_name

def make_label_bold(label):
    bf_label = '$\\bf{*' + label + '}$'
    return bf_label

#------------------------------------------------------------------
def configure_features_axis(features, strong, ax1 = None):
    '''
    '''
    feature_labels, split_points, group_labels, group_centres = get_feature_labels(
        features)
    n_features = len(features)

    for feat in strong:
        feature_labels[feat] = make_label_bold(feature_labels[feat])

    if ax1 is None:
        ax1 = plt.gca()

    ax1.set_xticks(np.arange(n_features))
    ax1.set_xticklabels(feature_labels, rotation='vertical', fontsize=14)
    ax1.set_xlim(-1, n_features)
    
    # Second X-axis
    ax2 = ax1.twiny()
    ax2.spines["bottom"].set_position(("axes", -0.12))
    ax2.tick_params('both', length=0, width=0, which='minor', labelsize=16)
    ax2.tick_params('both', direction='in', which='major')
    ax2.xaxis.set_ticks_position("bottom")
    ax2.xaxis.set_label_position("bottom")

    ax2.set_xticks(split_points)
    ax2.xaxis.set_major_formatter(ticker.NullFormatter())
    ax2.xaxis.set_minor_locator(ticker.FixedLocator(group_centres))
    ax2.xaxis.set_minor_formatter(ticker.FixedFormatter(group_labels))

#------------------------------------------------------------------
def plot_icc_data(icc_data, map_name, save_path=None, 
    strong_thresh=None, line_thresh=None):
    '''
    Plot ICC values for all features a bar chart with error bars

    For ISMRM abstract, call with strong_thresh = 0.7, line_thresh = 0.5
    '''
    map_icc_data = icc_data[map_name]
    features = map_icc_data.keys()
    feature_colors = get_feature_colors(features)
    
    n_features = len(features)
    icc_vals = np.zeros((n_features))
    icc_errs = np.zeros((2,n_features))
    for i_f,feature in enumerate(map_icc_data.keys()):
        feature_icc = map_icc_data[feature]
        if feature_icc is not None:
            icc_vals[i_f] = feature_icc['icc']
            icc_errs[0,i_f] = feature_icc['icc']-feature_icc['CI_lo']
            icc_errs[1,i_f] = feature_icc['CI_hi']-feature_icc['icc']

    if strong_thresh is None:
        strong_thresh = map_icc_data['original_firstorder_Median']['icc']
    
    if line_thresh is None:
        line_thresh = map_icc_data['original_firstorder_Median']['icc']

    strong = np.nonzero(icc_vals > strong_thresh)[0].tolist()
    
    plt.figure(figsize=[24,6])
    plt.bar(
        x = np.arange(n_features),
        height = icc_vals,
        yerr = icc_errs,
        color = feature_colors
    )
    plt.plot([-1,n_features], [line_thresh,line_thresh], 'g--', linewidth=3)
    
    #plt.title(f'{style_map_name(map_name)}: baseline ICC', fontsize=24)
    plt.ylim(0, 1)
    plt.ylabel('ICC (no units)', fontsize=20)
    configure_features_axis(features, strong)

    if save_path:
        plt.savefig(save_path, bbox_inches='tight', facecolor='w')
        features_file = os.path.join(
            os.path.splitext(save_path)[0] + '_features.txt'
        )
        with open(features_file, "wt") as file:
            for f in strong:
                print(f'{list(features)[f]}: {icc_vals[f]}', file=file)
            print(f'Min: {np.min(icc_vals)}', file=file)
            print(f'Max: {np.max(icc_vals)}', file=file)
            print(f'Num > {strong_thresh}: {len(strong)}', file=file)
    else:
        plt.show()

    return icc_vals, icc_errs

#------------------------------------------------------------------
def plot_wCV_data(wcv_data, map_name, save_path=None, 
    line_thresh=None):
    '''
    Plot wCV values for all features a bar chart
    '''
    map_wcv_data = wcv_data[map_name]
    features = map_wcv_data.keys()
    feature_colors = get_feature_colors(features)
    
    n_features = len(features)
    wcv_vals = np.zeros((n_features))
    for i_f,feature in enumerate(map_wcv_data.keys()):
        feature_wcv = map_wcv_data[feature]
        if feature_wcv is not None:
            wcv_vals[i_f] = 100*feature_wcv['wcv']
    
    plt.figure(figsize=[24,6])
    plt.bar(
        x = np.arange(n_features),
        height = wcv_vals,
        color = feature_colors
    )
    plt.plot([-1,n_features], [line_thresh,line_thresh], 'g--', linewidth=3)
    
    #plt.title(f'{style_map_name(map_name)}: baseline wCV', fontsize=24)
    plt.ylabel('wCV %', fontsize=20)
    plt.ylim(0, 100)
    configure_features_axis(features, [])

    median_wCV = 100*map_wcv_data['original_firstorder_Median']['wcv']
    print(f'baseline wCV for median {map_name}: {median_wCV}')
    print(f'Max wCV = {wcv_vals.max()}')

    if save_path:
        plt.savefig(save_path, bbox_inches='tight', facecolor='w')
        
    else:
        plt.show()

    return wcv_vals

#------------------------------------------------------------------
def plot_treatment_effect_data(treatment_effect_data, map_name, title_info, 
    save_path=None, strong_thresh=1.0,
    feature_type = 'median'):
    '''
    Plot treatment_effect for all features a bar chart with error bars

    For ISMRM abstract run with strong_thresh = 0.5
    '''
    map_treatment_effect_data = treatment_effect_data[map_name]
    features = map_treatment_effect_data.keys()
    feature_colors = get_feature_colors(features)

    n_features = len(features)
    treatment_effect_vals = np.zeros((n_features))
    treatment_effect_errs = np.zeros((n_features))

    if feature_type == 'median':
        base_effect = map_treatment_effect_data['original_firstorder_Median']

    else:
        base_effect = map_treatment_effect_data['original_shape_MeshVolume']

    base_mean = base_effect['mean']
    base_95 = 1.96*base_effect['se']
    base_pos_lo = -base_mean - base_95
    base_pos_hi = -base_mean + base_95
    base_neg_lo = base_mean + base_95
    base_neg_hi = base_mean - base_95


    plt.figure(figsize=[24,6])
    for i_f,feature in enumerate(map_treatment_effect_data.keys()):
        feature_treatment_effect = map_treatment_effect_data[feature]
        if feature_treatment_effect is not None:
            treatment_effect_vals[i_f] = feature_treatment_effect['mean']
            treatment_effect_errs[i_f] = 1.96*feature_treatment_effect['se']

        plt.errorbar(
            x = i_f,
            y = treatment_effect_vals[i_f],
            yerr = treatment_effect_errs[i_f],
            fmt = 'o',
            ecolor=feature_colors[i_f],
            elinewidth = 3,
            markersize = 12,
            mfc = feature_colors[i_f],
            mec = feature_colors[i_f]
        )

    strong = list()#np.nonzero(
        #np.abs(treatment_effect_vals / base_mean) > strong_thresh)[0].tolist()


    plt.plot([-1, n_features], [0, 0], 'k')
    #plt.plot(
    #    [-1, n_features], 
    #    [strong_thresh*base_mean, strong_thresh*base_mean], 'g--')
    #plt.plot(
    #    [-1, n_features], 
    #    [-strong_thresh*base_mean, -strong_thresh*base_mean], 'g--')


    plt.fill_between(
        [-1, n_features], base_pos_lo, base_pos_hi, color='y', alpha = 0.1,
        hatch = '/')
    plt.fill_between(
        [-1, n_features], base_pos_hi, 2.5, color='g', alpha = 0.1,
        hatch = '\\')
    plt.fill_between(
        [-1, n_features], base_neg_lo, base_neg_hi, color='y', alpha = 0.1,
        hatch = '/')
    plt.fill_between(
        [-1, n_features], base_neg_hi, -2.5, color='g', alpha = 0.1,
        hatch = '\\')
        
    plt.ylim(-2.5, 2.5)
    if not title_info is None:
        plt.title(f'{style_map_name(map_name)}: normalised treatment effects, {title_info}',
            fontsize=24)
    plt.ylabel('N\'malized $\Delta$feature (no units)', fontsize=20, labelpad = 4)
    plt.yticks(fontsize = 16)
    configure_features_axis(features, strong)

    if save_path:
        plt.savefig(save_path, bbox_inches='tight', facecolor='w')

        features_file = os.path.join(
            os.path.splitext(save_path)[0] + '_features.txt'
        )
        with open(features_file, "wt") as file:
            for f in strong:
                print(f'{list(features)[f]}: {treatment_effect_vals[f]}', file=file)
    else:
        plt.show()
        
    return treatment_effect_vals, treatment_effect_errs

#------------------------------------------------------------------
def plot_treatment_tumours_data(treatment_tumours_data, map_name, title_info, save_path=None, feature_type = 'median'):
    '''
    Plot treatment_tumours for all features a bar chart with error bars
    '''
    map_treatment_tumours_data = treatment_tumours_data[map_name]
    features = map_treatment_tumours_data.keys()
    feature_colors = get_feature_colors(features)

    n_features = len(features)
    treatment_tumours_vals = np.zeros((n_features))

    if feature_type == 'median':
        base_feature = 'original_firstorder_Median'
    else:
        base_feature = 'original_shape_MeshVolume'

    base_effect = map_treatment_tumours_data[base_feature]['num_change_patients']
    in_f_not_base = np.zeros((n_features))

    base_list = map_treatment_tumours_data[base_feature]\
        ['change_patients_list']

    for i_f,feature in enumerate(map_treatment_tumours_data.keys()):
        feature_treatment_tumours = map_treatment_tumours_data[feature]
        if feature_treatment_tumours is not None:
            treatment_tumours_vals[i_f] = feature_treatment_tumours['num_change_patients']
            
            f_list = feature_treatment_tumours['change_patients_list']
            
            f_extra = np.setdiff1d(f_list, base_list)
            in_f_not_base[i_f] = len(f_extra)
            
    strong = list()#np.nonzero(treatment_tumours_vals > base_effect)[0].tolist()

    plt.figure(figsize=[24,8])
    plt.bar(
        x = np.arange(n_features),
        height = treatment_tumours_vals,
        color = feature_colors,
        alpha = 0.5
    )
    plt.bar(
        x = np.arange(n_features),
        height = in_f_not_base,
        color = feature_colors
    )
    plt.plot([-1, n_features], [base_effect, base_effect], 'g--')
    plt.ylim([0, 45])
    plt.yticks(np.arange(0, 50, 5))

    plt.ylabel('# Tumours', fontsize=20, labelpad = 12)
    plt.yticks(fontsize = 16)
    if not title_info is None:
        plt.title(
            f'{style_map_name(map_name)}: number of tumours that significantly changed ({title_info})',
            fontsize=24)
    configure_features_axis(features, strong)
    
    if save_path:
        plt.savefig(save_path, bbox_inches='tight', facecolor='w')
        features_file = os.path.join(
            os.path.splitext(save_path)[0] + '_features.txt'
        )
        with open(features_file, "wt") as file:
            for f in strong:
                print(f'{list(features)[f]}: {treatment_tumours_vals[f]}', file=file)

    else:
        plt.show()

    return treatment_tumours_vals#, treatment_tumours_errs

#------------------------------------------------------------------
def plot_treatment_change_overlap_data(treatment_tumours_data, map_name, title_info, save_path=None):
    '''
    Plot treatment_tumours for all features a bar chart with error bars
    '''
    map_treatment_tumours_data = treatment_tumours_data[map_name]
    features = map_treatment_tumours_data.keys()
    feature_colors = get_feature_colors(features)

    n_features = len(features)
    in_f_not_median = np.zeros((n_features))

    median_list = map_treatment_tumours_data['original_firstorder_Median']\
        ['change_patients_list']
    
    
    for i_f, feature in enumerate(features):
        feature_treatment_tumours = map_treatment_tumours_data[feature]
        if feature_treatment_tumours is not None:
            f_list = feature_treatment_tumours['change_patients_list']
            
            f_extra = np.setdiff1d(f_list, median_list)
            in_f_not_median[i_f] = len(f_extra)

    plt.figure(figsize=[24,6])
    plt.bar(
        x = np.arange(n_features),
        height = in_f_not_median,
        color = feature_colors
    )
    plt.ylabel('# Tumours', fontsize=20)
    plt.yticks(fontsize = 16)
    plt.title(
        f'{style_map_name(map_name)}: number of tumours, with no median change, that significantly changed ({title_info})', 
        fontsize=24)
    configure_features_axis(features, [])
    
    if save_path:
        plt.savefig(save_path, bbox_inches='tight', facecolor='w')
    else:
        plt.show()

    return

def confidence_ellipse(x, y, ax, n_std=3.0, facecolor='none', **kwargs):
    """
    Create a plot of the covariance confidence ellipse of `x` and `y`

    Parameters
    ----------
    x, y : array_like, shape (n, )
        Input data.

    ax : matplotlib.axes.Axes
        The axes object to draw the ellipse into.

    n_std : float
        The number of standard deviations to determine the ellipse's radiuses.

    Returns
    -------
    matplotlib.patches.Ellipse

    Other parameters
    ----------------
    kwargs : `~matplotlib.patches.Patch` properties
    """
    if x.size != y.size:
        raise ValueError("x and y must be the same size")

    cov = np.cov(x, y)
    pearson = cov[0, 1]/np.sqrt(cov[0, 0] * cov[1, 1])
    # Using a special case to obtain the eigenvalues of this
    # two-dimensionl dataset.
    ell_radius_x = np.sqrt(1 + pearson)
    ell_radius_y = np.sqrt(1 - pearson)
    ellipse = Ellipse((0, 0),
        width=ell_radius_x * 2,
        height=ell_radius_y * 2,
        facecolor=facecolor,
        **kwargs)

    # Calculating the stdandard deviation of x from
    # the squareroot of the variance and multiplying
    # with the given number of standard deviations.
    scale_x = np.sqrt(cov[0, 0]) * n_std
    mean_x = np.mean(x)

    # calculating the stdandard deviation of y ...
    scale_y = np.sqrt(cov[1, 1]) * n_std
    mean_y = np.mean(y)

    transf = transforms.Affine2D() \
        .rotate_deg(45) \
        .scale(scale_x, scale_y) \
        .translate(mean_x, mean_y)

    ellipse.set_transform(transf + ax.transData)
    return ax.add_patch(ellipse)

#------------------------------------------------------------------
def plot_treatment_change_overlap_feature(
    rad_data, treatment_tumours_data, map_name, 
    treatment_visit, tumours, apply_boxcox, title_info, save_path=None):
    '''
    Plot treatment_tumours for all features a bar chart with error bars
    '''
    map_treatment_tumours_data = treatment_tumours_data[map_name]
    features = map_treatment_tumours_data.keys()
    feature_labels = get_feature_labels(features)[0]
    
    for i_f,feature in enumerate(features):
        feature_color = get_feature_colors([feature])
        group_label = feature.split('_')[1].upper()

        if group_label == 'SHAPE':
            group_label = 'Shape'
        elif group_label == 'FIRSTORDER':
            group_label = 'First-order'

        feature_label = f'{group_label} {feature_labels[i_f]}'

        m_cd = map_treatment_tumours_data['original_firstorder_Median']
        m_rc = np.ones((2,1))*m_cd['RC']
        m_rc0 = m_rc[0,0]
        m_list = m_cd['change_patients_list']

        f_cd = map_treatment_tumours_data[feature]
        f_rc = np.ones((2,1))*f_cd['RC']
        f_rc0 = f_rc[0,0]
        f_list = f_cd['change_patients_list']

        mf_list = np.intersect1d(m_list, f_list)
        mo_list = np.setdiff1d(m_list, f_list)
        fo_list = np.setdiff1d(f_list, m_list)

        m12, m_treatment, _, _, _ = dr.get_treatment_delta(
            rad_data, map_name, 'original_firstorder_Median', 
            treatment_visit, tumours, apply_boxcox)
        m_diff = m_treatment - np.mean(m12, axis=1)

        f12, f_treatment, _, _, _ = dr.get_treatment_delta(
            rad_data, map_name, feature, 
            treatment_visit, tumours, apply_boxcox)
        f_diff = f_treatment - np.mean(f12, axis=1)

        xmax = 3*m_rc[0,0]
        ymax = 3*f_rc[0,0]

        xlims = [-xmax, xmax]
        ylims = [-ymax, ymax]

        m_mean = np.nanmean(m_diff)
        m_std = np.nanstd(m_diff)
        
        f_mean = np.nanmean(f_diff)
        f_std = np.nanstd(f_diff)
        nf = np.sum(np.isfinite(f_diff))
        f_serr = 1.96*f_std / np.sqrt(nf)

        valid = np.isfinite(m_diff) & np.isfinite(f_diff)
        rho = np.corrcoef(m_diff[valid], f_diff[valid])[0,1]
        
        rc = '$\\it{RC}$' 
        xticks = [-3*m_rc0, -m_rc0, 0, m_rc0, 3*m_rc0]
        yticks = [-3*f_rc0, -f_rc0, 0, f_rc0, 3*f_rc0]
        tick_labels = [f'-3{rc}', f'-{rc}', 0, rc, f'3{rc}']

        plt.figure(figsize=[17,8])
        plt.suptitle(f'Treatment effect in {feature_label} vs median '
            f'{style_map_name(map_name)} ({title_info})',
            fontsize = 20)

        plt.subplot(1,2,1)
        h1, = plt.plot(m_diff, f_diff, '.', 
            markerfacecolor = feature_color[0],
            markeredgecolor = feature_color[0],
            markersize=8, zorder = 1, alpha = 0.5)

        h2, = plt.plot([m_mean, m_mean], [f_mean-f_serr, f_mean+f_serr], '-', 
            linewidth=3, zorder = 3, color = feature_color[0])

        confidence_ellipse(m_diff[valid], f_diff[valid], 
            plt.gca(), n_std=2.0,
            edgecolor=feature_color[0], linewidth=3)

        h3, = plt.plot(m_mean, f_mean, 'o', 
            markerfacecolor = feature_color[0],
            markeredgecolor = 'k',
            markersize=16, zorder = 4)  

        plt.plot(xlims, [0,0], 'k-', linewidth = 1.0, zorder=-1, alpha=0.5)
        plt.plot([0,0], ylims, 'k-', linewidth = 1.0, zorder=-1, alpha=0.5)

        plt.fill_between([-xmax, 0, xmax], [-ymax, 0, -ymax], -ymax, 
            color='m', alpha = 0.2, hatch = 'x')
        plt.fill_between([-xmax, 0, xmax], [ ymax, 0,  ymax],  ymax, 
            color='m', alpha = 0.2, hatch = 'x')

        plt.xlim(-xmax, xmax)
        plt.ylim(-ymax, ymax)

        plt.legend((h1,h3,h2), 
            ['Tumour $\Delta$', 'Cohort mean', 
            f'Corr. $\\rho$ = {rho:4.3f}'])
        plt.title('Cohort level')

        plt.xticks(xticks, labels = tick_labels, fontsize = 14)
        plt.yticks(yticks, labels = tick_labels, fontsize = 14)
        plt.xlabel(f'$\Delta$median {style_map_name(map_name)}', fontsize = 18)
        plt.ylabel(f'$\Delta${feature_label}', fontsize = 18)

        plt.subplot(1,2,2)
        h1, = plt.plot(m_diff, f_diff, '.', 
            markerfacecolor = feature_color[0],
            markeredgecolor = feature_color[0],
            markersize=8)
        h2, = plt.plot(m_diff[mo_list], f_diff[mo_list], '^', 
            markersize=8, 
            markerfacecolor = feature_color[0],
            markeredgecolor = feature_color[0])
        h3, = plt.plot(m_diff[fo_list], f_diff[fo_list], 'v', 
            markersize=8, 
            markerfacecolor = feature_color[0],
            markeredgecolor = feature_color[0])
        h4, = plt.plot(m_diff[mf_list], f_diff[mf_list], '*', 
            markersize=10, 
            markerfacecolor = feature_color[0],
            markeredgecolor = feature_color[0])

        plt.plot(xlims, [0,0], 'k-', linewidth = 1.0, alpha=0.5)
        plt.plot([0,0], ylims, 'k-', linewidth = 1.0, alpha=0.5)

        plt.plot(xlims, f_rc, 'k--', linewidth = 1.0)
        plt.plot(xlims, -f_rc, 'k--', linewidth = 1.0)
        plt.plot(m_rc, ylims, 'k--', linewidth = 1.0)
        plt.plot(-m_rc, ylims, 'k--', linewidth = 1.0)

        plt.fill_between([-xmax, -m_rc0], -ymax, -f_rc0, color='g', alpha = 0.2, hatch = 'x')      
        plt.fill_between([-xmax, -m_rc0],  f_rc0,  ymax, color='g', alpha = 0.2, hatch = 'x')
        plt.fill_between([ xmax,  m_rc0], -ymax, -f_rc0, color='g', alpha = 0.2, hatch = 'x')
        plt.fill_between([ xmax,  m_rc0],  f_rc0,  ymax, color='g', alpha = 0.2, hatch = 'x')

        plt.fill_between([-xmax, -m_rc0], -f_rc0,  f_rc0, color='c', alpha = 0.2, hatch = '\\')
        plt.fill_between([ xmax,  m_rc0], -f_rc0,  f_rc0, color='c', alpha = 0.2, hatch = '\\')

        plt.fill_between([-m_rc0, m_rc0], -ymax, -f_rc0, color='y', alpha = 0.2, hatch = '/')
        plt.fill_between([-m_rc0, m_rc0],  f_rc0,  ymax, color='y', alpha = 0.2, hatch = '/')

        plt.xlim(-xmax, xmax)
        plt.ylim(-ymax, ymax)
        #plt.axis('equal')

        plt.legend((h1,h2,h3,h4), (
            f'Non-sig. change',
            f'Sig. change in median only',
            f'Sig. change in {feature_label} only',
            f'Sig. change in {feature_label} + median'
        ))
        plt.title('Individual tumours')
        plt.xticks(xticks, labels = tick_labels, fontsize = 14)
        plt.yticks(yticks, labels = tick_labels, fontsize = 14)
        plt.xlabel(f'$\Delta$median {style_map_name(map_name)}', fontsize = 18)
        plt.ylabel(f'$\Delta${feature_label}', fontsize = 18)
        
        if save_path:
            save_file = f'{save_path}_{feature_label}.png'
            plt.savefig(save_file, bbox_inches='tight', facecolor='w')
        else:
            plt.show()

        if i_f:
            plt.close()

#------------------------------------------------------------------
def plot_corr_data(corr_data, features, map_name, title_info, save_path=None):
    '''
    Plot corr for all features a bar chart with error bars
    '''
    map_corr_data = np.abs(corr_data[map_name]['corr_matrix'])
    for i_f in range(map_corr_data.shape[0]):
        map_corr_data[i_f,i_f] = np.nan

    
    feature_colors = get_feature_colors(features)
    feature_labels = [f.replace('original_','') for f in features]

    med_idx = feature_labels.index('firstorder_Median')
    feature_labels[med_idx] += ' *'

    n_features = len(features)

    cmap=plt.get_cmap('YlGn')
    cmap.set_bad('white')

    plt.figure(figsize=[28,28])
    plt.imshow(map_corr_data)

    plt.title(f'{style_map_name(map_name)}: correlation coefficient between features, {title_info}')
    plt.xticks(np.arange(n_features), feature_labels, rotation='vertical')
    plt.yticks(np.arange(n_features), feature_labels, rotation='horizontal')

    gca = plt.gca()
    for i_f,(x_label,y_label) in enumerate(zip(gca.get_xticklabels(), gca.get_yticklabels())):
        x_label.set_color(feature_colors[i_f])
        y_label.set_color(feature_colors[i_f])
        if i_f == med_idx:
            x_label.set_weight('bold')
            y_label.set_weight('bold')

    
    if save_path:
        plt.savefig(save_path, bbox_inches='tight', facecolor='w')
    else:
        plt.show()

#------------------------------------------------------------------
def plot_median_correlation(corr_data, features,
    map_name, title_info, save_path=None, strong_thresh=0.05,
    feature_type = 'median'):
    '''
    Plot corr for all features a bar chart with error bars
    '''
    map_corr_data = corr_data[map_name]
    
    n_features = len(features)
    checklist_map = np.zeros((5,n_features))

    med_corr_vals = map_corr_data[f'{feature_type}_r']
    med_corr_sigs = map_corr_data[f'{feature_type}_p']
    strong = []#np.nonzero(med_corr_sigs > strong_thresh)[0].tolist()

    feature_colors = get_feature_colors(features)

    plt.figure(figsize=[24,6])
    plt.rc('ytick', labelsize=18)
    plt.bar(
        x = np.arange(n_features),
        height = np.abs(med_corr_vals),
        #yerr = corr_errs,
        color = feature_colors
    )
    
    plt.plot([-1,n_features], [0,0], 'k-', linewidth=1.0)
    #plt.text(14.0, -0.9, 'Corr. to tumour volume', fontsize=20)
    #plt.text(1.0, 0.8, f'Corr. to median {style_map_name(map_name)}', fontsize=20)
    
    #plt.plot([-1,n_features], [0.5,0.5], 'g--', linewidth=3)
    #plt.plot([-1,n_features], [-0.5,-0.5], 'g--', linewidth=3)
    plt.ylim(0, 1)
    if not title_info is None:
        plt.title(
            f'{style_map_name(map_name)}: correlation coefficient with tumour {feature_type}, {title_info}',
            fontsize=24)
    
    plt.ylabel('$|\\rho$| (no units)', fontsize=20, labelpad = 0)
    configure_features_axis(features, strong)

    if save_path:
        plt.savefig(save_path, bbox_inches='tight', facecolor='w')
        features_file = os.path.join(
            os.path.splitext(save_path)[0] + '_features.txt'
        )
        with open(features_file, "wt") as file:
            for f in strong:
                print(f'{list(features)[f]}: {med_corr_vals[f]}', file=file)

    else:
        plt.show()


    return checklist_map

#------------------------------------------------------------------
def make_checklist_map(icc_data, treatment_effect_data, corr_data, 
    map_name, save_path=None, strong_thresh = 0.5):
    '''
    Plot corr for all features a bar chart with error bars
    '''
    map_icc_data = icc_data[map_name]
    map_treatment_effect_data1 = treatment_effect_data[0][map_name]
    map_treatment_effect_data2 = treatment_effect_data[1][map_name]
    map_corr_data1 = corr_data[0][map_name]
    map_corr_data2 = corr_data[1][map_name]
    
    features = map_icc_data.keys()
    n_features = len(features)
    checklist_map = np.zeros((5,n_features))

    median_effect1 = abs(map_treatment_effect_data1['original_firstorder_Median']['mean'])
    median_effect2 = abs(map_treatment_effect_data2['original_firstorder_Median']['mean'])

    
    for i_f,feature in enumerate(features):
        checklist_map[0,i_f] = map_icc_data[feature]['icc']
        checklist_map[1,i_f] = abs(map_treatment_effect_data1[feature]['mean']) / median_effect1
        checklist_map[2,i_f] = abs(map_treatment_effect_data2[feature]['mean']) / median_effect2   
        
    checklist_map[3,:] = 1 - np.abs(map_corr_data1['median_r'])
    checklist_map[4,:] = 1 - np.abs(map_corr_data1['median_r'])

    all_strong = np.nonzero(
        np.all(checklist_map > strong_thresh, axis=0))[0].tolist()

    axis_labels = [
        'Baseline ICC', 
        '48 hrs effect size', 
        'EC1 effect size',
        '48 hrs corr to med.',
        'EC1 corr to med.']

    plt.figure(figsize=[24,6])
    plt.imshow(checklist_map, 
        aspect='auto',
        cmap = plt.get_cmap('RdYlGn'),
        vmin = 0,
        vmax = 1)
    plt.title(f'{style_map_name(map_name)}: check list of desirable properties', fontsize=24)
    plt.yticks(np.arange(5), axis_labels, rotation='horizontal', fontsize=18)
    
    configure_features_axis(features, all_strong)

    if save_path:
        plt.savefig(save_path, bbox_inches='tight', facecolor='w')
    else:
        plt.show()

    features_file = os.path.join(
        os.path.splitext(save_path)[0] + '_features.txt'
    )
    with open(features_file, "wt") as file:
        for feat in all_strong:
            print(f'{list(features)[feat]}: {checklist_map[:,feat]}', file=file)

    return checklist_map

#------------------------------------------------------------------
def make_ranking_map(icc_data, treatment_effect_data, 
    treatment_tumours_data, corr_data, 
    map_name, save_path=None, strong_thresh = 0.5):
    '''
    Plot corr for all features a bar chart with error bars
    '''
    map_icc_data = icc_data[map_name]
    map_treatment_effect_data = []
    map_treatment_tumours_data = []
    map_corr_data = []
    median_list = []
    for t in [0, 1]:
        map_treatment_effect_data.append(treatment_effect_data[t][map_name])
        map_treatment_tumours_data.append(treatment_tumours_data[t][map_name])
        map_corr_data.append(corr_data[t][map_name])
        median_list.append(map_treatment_tumours_data[t]['original_firstorder_Median']\
            ['change_patients_list'])

    features = map_icc_data.keys()
    n_features = len(features)
    n_rows = 9
    checklist_scores = np.zeros((n_rows,n_features))
    
    for i_f,feature in enumerate(features):
        checklist_scores[0,i_f] = map_icc_data[feature]['icc']

        row = 0
        for t in [0,1]:
            row += 1
            checklist_scores[row,i_f] = abs(map_treatment_effect_data[t][feature]['mean'])
        
            feature_treatment_tumours = map_treatment_tumours_data[t][feature]
            if feature_treatment_tumours is not None:
                f_list = feature_treatment_tumours['change_patients_list']      
                f_extra = np.setdiff1d(f_list, median_list[t])

                row += 1
                checklist_scores[row,i_f] = feature_treatment_tumours['num_change_patients']
                row += 1
                checklist_scores[row,i_f] = len(f_extra)
                row += 1 #To fit in corr data not inserted in loop
        
    checklist_scores[4,:] = -np.abs(map_corr_data[0]['median_r'])
    checklist_scores[8,:] = -np.abs(map_corr_data[1]['median_r'])
    checklist_order = np.argsort(checklist_scores, axis = 1)
    
    checklist_map = 100*np.argsort(checklist_order, axis = 1)/n_features
    checklist_map_48h = checklist_map[(0,1,4,2,3),:]
    checklist_map_EC1 = checklist_map[(0,5,8,6,7),:]

    all_strong_48h = np.nonzero(
        np.all(checklist_map_48h > strong_thresh*100, axis=0))[0].tolist()
    all_strong_EC1 = np.nonzero(
        np.all(checklist_map_EC1 > strong_thresh*100, axis=0))[0].tolist()

    axis_labels = [
        [
        'Baseline ICC', 
        '48 hrs cohort effect',
        '48 hrs corr to med.',
        '48 hrs changing tumours', 
        '48 hrs (feature-only)'],
        [
        'Baseline ICC', 
        'EC1 cohort effect',
        'EC1 corr to med.',
        'EC1 changing tumours', 
        'EC1 (feature-only)']]

    fig, ax = plt.subplots(2, 1, figsize=[24,9], constrained_layout = True)
    im = ax[0].imshow(checklist_map_48h, 
        aspect='auto',
        cmap = plt.get_cmap('RdYlGn'),
        vmin = 0,
        vmax = 100)
    
    #ax[0].set_title(f'{style_map_name(map_name)}: ranking map of desirable properties', 
    #    {'fontsize':24})
    ax[0].set_yticks(np.arange(5))
    ax[0].set_yticklabels(axis_labels[0], {'fontsize':18})
    configure_features_axis(features, all_strong_48h, ax[0])

    ax[1].imshow(checklist_map_EC1, 
        aspect='auto',
        cmap = plt.get_cmap('RdYlGn'),
        vmin = 0,
        vmax = 100)
    ax[1].set_yticks(np.arange(5))
    ax[1].set_yticklabels(axis_labels[1], {'fontsize':18})
    configure_features_axis(features, all_strong_EC1, ax[1])

    fig.colorbar(im, ax = ax[:], pad = 0.01, aspect=50)
    

    if save_path:
        plt.savefig(save_path, bbox_inches='tight', facecolor='w')
    else:
        plt.show()

    features_file = os.path.join(
        os.path.splitext(save_path)[0] + '_features.txt'
    )
    
    with open(features_file, "wt") as file:
        print('48h', file=file)
        for feat in all_strong_48h:
            print(f'{list(features)[feat]}: {checklist_scores[:,feat]}', file=file)
        print('EC1', file=file)
        for feat in all_strong_EC1:
            print(f'{list(features)[feat]}: {checklist_scores[:,feat]}', file=file)
    

    return checklist_scores

#------------------------------------------------------------------
def make_median_stats_plot(rad_data, map_name, map_units, 
    visits=[1,3,4], tumours = 'all_tumours', apply_boxcox = True, save_path=None,
    make_baseline_scatter = False,
    make_baseline_bland = True,
    make_all_visits_boxplot = False,
    make_all_visits_violin = False,
    make_treatment_boxplot = False,
    make_treatment_violin = False,
    make_treatment_lineplot = True,
    feature_type = 'median'):

    if feature_type == 'median':
        feature = 'original_firstorder_Median'
    else:
        feature = 'original_shape_MeshVolume'

    #Get tumour data for medians
    rad_tumour_fun = dr.select_tumour_fun(tumours)
    
    v1234_data_trav = rad_tumour_fun(rad_data[0],
        map_name, feature, [1,2,3,4]) / 100
    v1234_data_beva = rad_tumour_fun(rad_data[1],
        map_name, feature, [1,2,3,4]) / 100

    beva_total = np.sum(np.isfinite(v1234_data_beva), axis = 0)
    trav_total = np.sum(np.isfinite(v1234_data_trav), axis = 0)
    beva_baseline1 = np.sum(
        np.any(np.isfinite(v1234_data_beva[:,:2]), axis=1), axis = 0)
    beva_baseline2 = np.sum(
        np.all(np.isfinite(v1234_data_beva[:,:2]), axis=1), axis = 0)
    trav_baseline1 = np.sum(
        np.any(np.isfinite(v1234_data_trav[:,:2]), axis=1), axis = 0)
    trav_baseline2 = np.sum(
        np.all(np.isfinite(v1234_data_trav[:,:2]), axis=1), axis = 0)

    print(f'Bevacizumab tumour counts by visit: {beva_total}')
    print(f'Bevacizumab any/both baseline counts: {beva_baseline1}, {beva_baseline2}')

    print(f'Travastin tumour counts: {trav_total}')
    print(f'Travastin any/both baseline counts: {trav_baseline1}, {trav_baseline2}')

    n_trav = v1234_data_trav.shape[0]

    v1234_data = np.concatenate((v1234_data_trav,v1234_data_beva),axis=0)

    v1_orig = np.copy(v1234_data[:,0])

    #Apply box-cox transform
    if apply_boxcox:
        f_min = v1234_data[np.isfinite(v1234_data)].min()
        lmbda = dr.boxcox_lambda(v1234_data)
        dr.boxcox_transform(v1234_data, lmbda)

    v_data_trav = []
    v_data_beva = []
    v_data_all = []

    dv_data_trav = []
    dv_data_beva = []
    dv_data_all_valid = []
    dv_data_all = []

    dv_data_trav_mean = [0]
    dv_data_beva_mean = [0]
    dv_data_all_mean = [0]

    dv_data_trav_serr = [0]
    dv_data_beva_serr = [0]
    dv_data_all_serr = [0]

    p_trav = []
    p_beva = []
    p_all = []

    min_v = 0
    max_v = 0
    max_dv = 0

    v_labels = ('Avg. baseline', '48 hrs', 'EC1')

    #Get baseline visit data
    for visit in visits:
        if visit == 1:
            v_trav = v1234_data[0:n_trav,0:2]
            v_beva = v1234_data[n_trav:,0:2]
            v12_trav = v_trav
            v12_beva = v_beva

            v12_all = np.concatenate((v12_trav, v12_beva), axis=0)
            v12_diff = np.diff(v12_all,axis=1)
            v12_diff_mean = np.nanmean(v12_diff)
            v12_diff_std = np.nanstd(v12_diff)
            v12_UI = v12_diff_mean + 1.96*v12_diff_std
            v12_LI = v12_diff_mean - 1.96*v12_diff_std

        else:
            
            v_trav = v1234_data[0:n_trav,visit-1]
            v_beva = v1234_data[n_trav:,visit-1]

            print(v12_trav.shape)
            print(v12_beva.shape)

            dv_trav = v_trav - np.nanmean(v12_trav, axis=1)
            dv_beva = v_beva - np.nanmean(v12_beva, axis=1)
            dv_data_all.append(
                np.concatenate((dv_trav,dv_beva), axis=0))
            
            valid_trav = np.isfinite(dv_trav)
            valid_beva = np.isfinite(dv_beva)
            dv_data_trav.append(dv_trav[valid_trav])
            dv_data_beva.append(dv_beva[valid_beva])
            dv_data_all_valid.append(
                np.concatenate((dv_data_trav[-1],dv_data_beva[-1]), axis=0))

            dv_mean_trav = np.mean(dv_data_trav[-1])
            dv_serr_trav = np.std(dv_data_trav[-1]) / np.sqrt(np.sum(valid_trav))

            dv_mean_beva = np.mean(dv_data_beva[-1])
            dv_serr_beva = np.std(dv_data_beva[-1]) / np.sqrt(np.sum(valid_beva))

            dv_mean_all = np.mean(dv_data_all_valid[-1])
            dv_serr_all = np.std(dv_data_all_valid[-1]) / np.sqrt(np.sum(valid_beva) + np.sum(valid_trav))

            dv_data_trav_mean.append(dv_mean_trav)
            dv_data_beva_mean.append(dv_mean_beva)
            dv_data_all_mean.append(dv_mean_all)

            dv_data_trav_serr.append(dv_serr_trav)
            dv_data_beva_serr.append(dv_serr_beva)
            dv_data_all_serr.append(dv_serr_all)

            max_dv = max(max_dv, np.max(np.abs(dv_data_all_valid[-1])))

            p_trav.append(ttest_1samp(dv_data_trav[-1], 0, nan_policy='omit')[1])
            p_beva.append(ttest_1samp(dv_data_beva[-1], 0, nan_policy='omit')[1])
            p_all.append(ttest_1samp(dv_data_all[-1], 0, nan_policy='omit')[1])


        valid_trav = np.isfinite(v_trav)
        valid_beva = np.isfinite(v_beva)      

        v_data_trav.append(v_trav[valid_trav])
        v_data_beva.append(v_beva[valid_beva])

        v_data_all.append(
            np.concatenate((v_data_trav[-1],v_data_beva[-1]), axis=0))
        min_v = min(min_v, np.min(v_data_all[-1]))
        max_v = max(max_v, np.max(v_data_all[-1]))

        print(v_data_trav[-1].shape)

    v12_min = np.nanmin(v12_all)
    v12_max = np.nanmax(v12_all)

    #-----------------------------------------------------------
    if make_baseline_scatter:
        plt.figure(figsize=(8,8))
        h1, = plt.plot(v12_beva[:,0], v12_beva[:,1], 'bo')
        h2, = plt.plot(v12_trav[:,0], v12_trav[:,1], 'ro')
        plt.plot([0, max_v], [0, max_v], 'k--')
        plt.xlim(0, max_v)
        plt.ylim(0, max_v)
        plt.axis('equal')
        #plt.title(f'{map_name}: tumour medians at baseline')
        plt.xlabel(f'Visit 1 - {map_units}')
        plt.ylabel(f'Visit 2 - {map_units}')
        plt.legend((h1,h2), ('Study 1 tumours', 'Study 2 tumours'))
        if save_path:
            plt.savefig(save_path + '_baseline_scatter.png', bbox_inches='tight', facecolor='w')
        else:
            plt.show()

    #-----------------------------------------------------------
    if make_baseline_bland:
        plt.figure(figsize=(8,8))
        plt.rc('xtick', labelsize=18)
        plt.rc('ytick', labelsize=18)
        h1, = plt.plot(np.mean(v12_beva, axis=1), np.diff(v12_beva,axis=1), 'bo', markersize=8)
        h2, = plt.plot(np.mean(v12_trav, axis=1), np.diff(v12_trav,axis=1), 'ro', markersize=8)
        hmean, = plt.plot([v12_min, v12_max], [v12_diff_mean, v12_diff_mean], 'k--')
        hci, = plt.plot([v12_min, v12_max], [v12_UI, v12_UI], 'k-.')
        plt.plot([v12_min, v12_max], [v12_LI, v12_LI], 'k-.')
        #plt.xlim(min_v, max_v)
        #plt.ylim(2*v12_LI, 2*v12_UI)
        #plt.title(f'{style_map_name(map_name)}: tumour medians at baseline', fontsize=24)
        plt.xlabel(f'Avg. baseline', fontsize=20)# ({map_units})
        plt.ylabel(f'Baseline difference', fontsize=20)# ({map_units})
        plt.legend(
            (h1, h2, hmean, hci),
            ('Study 1 tumours', 'Study 2 tumours', 'Mean difference', '95% lims. of agr.'),
            fontsize=16)
        #if apply_boxcox:
        #    xt = plt.xticks()[0]
        #    yt = plt.yticks()[0]
        #    plt.xticks(xt, labels = dr.boxcox_inverse(xt, lmbda, f_min))
        #    plt.yticks(yt, labels = dr.boxcox_inverse(yt, lmbda, f_min))
        if save_path:
            plt.savefig(save_path + '_baseline_bland.png', bbox_inches='tight', facecolor='w')
        else:
            plt.show()

    #-----------------------------------------------------------
    if make_all_visits_boxplot:
        plt.figure(figsize=(18,6))
        plt.suptitle(f'{map_name}: distribution of tumour {feature_type}s by visit')
        plt.subplot(1,3,1)
        plt.boxplot(v_data_trav, labels=v_labels, bootstrap=1000)
        plt.title(f'Study 1')
        plt.ylabel(f'{map_name} ({map_units})')
        plt.ylim(min_v, max_v)

        plt.subplot(1,3,2)
        plt.boxplot(v_data_beva, labels=v_labels, bootstrap=1000)
        plt.title(f'Study 2')
        plt.ylabel(f'{map_name} ({map_units})')
        plt.ylim(min_v, max_v)

        plt.subplot(1,3,3)
        plt.boxplot(v_data_all, labels=v_labels, bootstrap=1000)
        plt.title(f'All patients ')
        plt.ylabel(f'{map_name} ({map_units})')
        plt.ylim(min_v, max_v)
        if save_path:
            plt.savefig(save_path + '_all_visits_boxplot.png', bbox_inches='tight', facecolor='w')
        else:
            plt.show()

    #-----------------------------------------------------------
    if make_all_visits_violin:
        plt.figure(figsize=(18,6))
        plt.suptitle(f'{map_name}: distribution of tumour {feature_type}s by visit')
        plt.subplot(1,3,1)
        plt.violinplot(v_data_trav)
        plt.title(f'Study 1')
        plt.ylabel(f'{map_name} ({map_units})')
        plt.ylim(min_v, max_v)
        plt.xticks([1,2,3],labels=v_labels)

        plt.subplot(1,3,2)
        plt.violinplot(v_data_beva)
        plt.title(f'Study 2')
        plt.ylabel(f'{map_name} ({map_units})')
        plt.ylim(min_v, max_v)
        plt.xticks([1,2,3],labels=v_labels)

        plt.subplot(1,3,3)
        plt.violinplot(v_data_all)
        plt.title(f'All patients ')
        plt.ylabel(f'{map_name} ({map_units})')
        plt.ylim(min_v, max_v)
        plt.xticks([1,2,3],labels=v_labels)
        if save_path:
            plt.savefig(save_path + '_all_visits_violin.png', bbox_inches='tight', facecolor='w')
        else:
            plt.show()

    #-----------------------------------------------------------
    if make_treatment_boxplot:
        plt.figure(figsize=(18,6))
        plt.suptitle(f'{map_name}: change in tumour {feature_type}s from baseline')
        plt.subplot(1,3,1)
        plt.boxplot(dv_data_trav, labels=v_labels[1:], bootstrap=1000)
        plt.plot([0.5,2.5], [0,0], 'k--')
        plt.title(f'Study 1 tumours')
        plt.ylabel(f'$\Delta${map_name} ({map_units})')
        plt.ylim(-max_dv, max_dv)

        plt.subplot(1,3,2)
        plt.boxplot(dv_data_beva, labels=v_labels[1:], bootstrap=1000)
        plt.plot([0.5,2.5], [0,0], 'k--')
        plt.title(f'Study 2 tumours')
        plt.ylabel(f'$\Delta${map_name} ({map_units})')
        plt.ylim(-max_dv, max_dv)

        plt.subplot(1,3,3)
        plt.boxplot(dv_data_all, labels=v_labels[1:], bootstrap=1000)
        plt.plot([0.5,2.5], [0,0], 'k--')
        plt.title(f'All tumours')
        plt.ylabel(f'$\Delta${map_name} ({map_units})')
        plt.ylim(-max_dv, max_dv)
        if save_path:
            plt.savefig(save_path + '_treatment_boxplot.png', bbox_inches='tight', facecolor='w')
        else:
            plt.show()

    #-----------------------------------------------------------
    if make_treatment_violin:
        plt.figure(figsize=(18,6))
        plt.suptitle(f'{map_name}: change in tumour {feature_type}s by visit')
        plt.subplot(1,3,1)
        plt.violinplot(dv_data_trav)
        plt.plot([0.5,2.5], [0,0], 'k--')
        plt.title(f'Study 1 tumours')
        plt.ylabel(f'$\Delta${map_name} ({map_units})')
        plt.ylim(-max_dv, max_dv)
        plt.xticks([1,2],labels=v_labels[1:])
        
        plt.subplot(1,3,2)
        plt.violinplot(dv_data_beva)
        plt.plot([0.5,2.5], [0,0], 'k--')
        plt.title(f'Study 2 tumours')
        plt.ylabel(f'$\Delta${map_name} ({map_units})')
        plt.ylim(-max_dv, max_dv)
        plt.ylim(-max_dv, max_dv)
        plt.xticks([1,2],labels=v_labels[1:])

        plt.subplot(1,3,3)
        plt.violinplot(dv_data_all)
        plt.plot([0.5,2.5], [0,0], 'k--')
        plt.title(f'All tumours')
        plt.ylabel(f'$\Delta${map_name} ({map_units})')
        plt.ylim(-max_dv, max_dv)
        plt.ylim(-max_dv, max_dv)
        plt.xticks([1,2],labels=v_labels[1:])
        if save_path:
            plt.savefig(save_path + '_treatment_violin.png', bbox_inches='tight', facecolor='w')
        else:
            plt.show()

    #----------------------------
    if make_treatment_lineplot:
        days = np.array([0, 2, 13])
        days_offset = np.array([0, 1, 1])
        dv_data_trav_mean = np.array(dv_data_trav_mean)
        dv_data_beva_mean = np.array(dv_data_beva_mean)
        dv_data_all_mean = np.array(dv_data_all_mean)

        dv_data_trav_serr = np.array(dv_data_trav_serr)
        dv_data_beva_serr = np.array(dv_data_beva_serr)
        dv_data_all_serr = np.array(dv_data_all_serr)

        bar_scale = np.array([-1.96, 1.96])
        unit_scale = np.array([1, 1])

        plt.figure(figsize=(12,8))
        plt.rc('xtick', labelsize=18)
        plt.rc('ytick', labelsize=18)
        
        h1, = plt.plot(days + 0.25*days_offset, dv_data_beva_mean, 'bo', markersize=12)
        h2, = plt.plot(days, dv_data_trav_mean, 'ro', markersize=12)
        h12, = plt.plot(days + 0.5*days_offset, dv_data_all_mean, 'gs', markersize=12)
        
        def p_string(p_val):
            p_it = '$\it{p}$'
            if p_val < 0.001:
                return f'{p_it} < 0.001'
            else:
                return f'{p_it} = {p_val:4.3f}'

        plt.legend((h1,h2,h12), 
            (
            f'Study 1 tumours (48h {p_string(p_beva[0])},  EC1 {p_string(p_beva[1])})', 
            f'Study 2 tumours (48h {p_string(p_trav[0])},  EC1 {p_string(p_trav[1])})', 
            f'All tumours (48h {p_string(p_all[0])},  EC1 {p_string(p_all[1])})'), 
            fontsize=20)

        plt.plot(days, dv_data_trav_mean, 'r--', linewidth=3.0)
        for visit in range(3):
            plt.plot(
                days[visit]*unit_scale, 
                dv_data_trav_mean[visit]+bar_scale*dv_data_trav_serr[visit], 
                'r-', linewidth=3.0)

        plt.plot(days + 0.25*days_offset, dv_data_beva_mean, 'b--', linewidth=3.0)
        for visit in range(3):
            plt.plot(
                days[visit]*unit_scale + 0.25*days_offset[visit], 
                dv_data_beva_mean[visit]+bar_scale*dv_data_beva_serr[visit], 
                'b-', linewidth=3.0)

        plt.plot(days + 0.5*days_offset, dv_data_all_mean, 'g--', linewidth=3.0)
        for visit in range(3):
            plt.plot(
                days[visit]*unit_scale + 0.5*days_offset[visit], 
                dv_data_all_mean[visit]+bar_scale*dv_data_all_serr[visit], 
                'g-', linewidth=3.0)

        #plt.title(f'{style_map_name(map_name)}: change in tumour medians by visit', fontsize=24)
        plt.ylabel(f'$\Delta${map_name}', fontsize=20) #({map_units})
        plt.xticks([0, 2.25, 13.25], labels = v_labels, fontsize=20)
        #if apply_boxcox:
        #    yt = plt.yticks()[0]
        #    plt.yticks(yt, labels = dr.boxcox_inverse(yt, lmbda, f_min))

        if save_path:
            plt.savefig(save_path + '_treatment_lineplot.png', bbox_inches='tight', facecolor='w')
        else:
            plt.show()

#------------------------------------------------------------------
def get_waterfall_colors(v_delta, RC):
    '''
    '''
    n_tumours = len(v_delta)
    blues = cm.get_cmap('Blues')
    reds = cm.get_cmap('Reds')
    n_reds = np.sum(v_delta <= -RC)
    n_blues = n_tumours - n_reds
    

    tumour_colours = ([blues(t) for t in np.linspace(0.25,1.0,n_blues)] + 
        [reds(t) for t in np.linspace(0.5,1.0,n_reds)])
    
    return tumour_colours


#------------------------------------------------------------------
def make_median_waterfall_plot(rad_data, map_name, 
    tumours = 'all_tumours', save_path=None, apply_boxcox = False,
    feature = 'original_firstorder_Median'):
    
    #Get tumour data for medians
    rad_tumour_fun = dr.select_tumour_fun(tumours)
    v1234_data = rad_tumour_fun(rad_data, map_name, feature, [1,2,3,4])
    
    if not 'Volume' in feature:
        v1234_data /= 100

    #Apply box-cox transform
    if apply_boxcox:
        f_min = v1234_data[np.isfinite(v1234_data)].min()
        lmbda = dr.boxcox_lambda(v1234_data)
        dr.boxcox_transform(v1234_data, lmbda)

    #Compute baseline difference and RC
    v12 = v1234_data[:,0:2]
    valid_baseline = np.all(np.isfinite(v12), axis=1)
    valid_all = np.all(np.isfinite(v12), axis=1) &\
        np.any(np.isfinite(v1234_data[:,2:3]), axis=1)

    #Compute Repeatability co-efficient from baseline diff
    baseline_diff = v12[valid_baseline,0] - v12[valid_baseline,1]
    RC = dr.compute_RC(baseline_diff)

    print(f'{map_name} RC = {RC}')

    #Compute baseline average and change from baseline at 48hrs and EC1
    baseline_mean = np.mean(v12[valid_all,:], axis = 1)

    dv_48 = v1234_data[:,2][valid_all] - baseline_mean
    dv_ec1 = v1234_data[:,3][valid_all] - baseline_mean

    print(f'Number of sig change at 48h {np.sum(np.abs(dv_48) > RC)}')
    print(f'({np.sum(dv_48 > RC)}, {np.sum(dv_48 < -RC)})')
    print(f'Number of sig change at EC1 {np.sum(np.abs(dv_ec1) > RC)}')
    print(f'({np.sum(dv_ec1 > RC)}, {np.sum(dv_ec1 < -RC)})')

    #Get indices required to sort differences at 48 hrs and EC1
    sorted_idx_48 = np.argsort(dv_48)[::-1]
    sorted_idx_ec1 = np.argsort(dv_ec1)[::-1]
    rank_48 = np.argsort(sorted_idx_48)
    rank_ec1 = np.argsort(sorted_idx_ec1)

    missing_48 = np.nonzero(~np.isfinite(dv_48[sorted_idx_48]))
    missing_ec1 = np.nonzero(~np.isfinite(dv_ec1[sorted_idx_48]))

    n_tumours = len(dv_48)

    #-----------------------------------------------------------
    tumour_colours_48 = get_waterfall_colors(dv_48, RC)
    tumour_colours_ec1 = get_waterfall_colors(dv_ec1, RC)
    colours_48 = [tumour_colours_48[t] for t in rank_48]
    colours_ec1 = [tumour_colours_ec1[t] for t in rank_ec1]

    #-----------------------------------------------------------
    plt.figure(figsize=(16,10))
    plt.subplot(2,1,1)
    plt.bar(
        x = np.arange(n_tumours),
        height = dv_48[sorted_idx_48],
        color = [colours_48[t] for t in sorted_idx_48])
    plt.plot([0, n_tumours], [RC, RC], 'k--')
    plt.plot([0, n_tumours], [-RC, -RC], 'k--')
    #plt.plot(missing_48, 0, 'ko')
    plt.ylim([-2.5*RC,2.5*RC])
    #if apply_boxcox:
    #    yt = plt.yticks()[0]
    #    plt.yticks(yt, labels = dr.boxcox_inverse(yt, lmbda, f_min))

    #plt.xlabel('Tumour #')
    if 'Volume' in feature:
        plt.ylabel(f'$\Delta$Volume at 48 hrs', fontsize=20)
    else:
        plt.ylabel(f'$\Delta${style_map_name(map_name)} at 48 hrs', fontsize=20)
    #plt.title(f'{style_map_name(map_name)}: change in tumour medians by visit', fontsize=24)

    plt.subplot(2,1,2)
    plt.bar(
        x = np.arange(n_tumours),
        height = dv_ec1[sorted_idx_48],
        color = [colours_ec1[t] for t in sorted_idx_48])
    plt.plot([0, n_tumours], [RC, RC], 'k--')
    plt.plot([0, n_tumours], [-RC, -RC], 'k--')
    plt.plot(missing_ec1, 0, 'ko')
    plt.ylim([-2.5*RC,2.5*RC])
    #if apply_boxcox:
    #    yt = plt.yticks()[0]
    #    plt.yticks(yt, labels = dr.boxcox_inverse(yt, lmbda, f_min))
    plt.xlabel('Tumours sorted by change from avg. baseline at 48 hrs', fontsize=20)
    if 'Volume' in feature:
        plt.ylabel(f'$\Delta$Volume at EC1', fontsize=20)
    else:
        plt.ylabel(f'$\Delta${style_map_name(map_name)} at EC1', fontsize=20)

    if save_path:
        plt.savefig(save_path + '.png', bbox_inches='tight', facecolor='w')
    else:
        plt.show()

#------------------------------------------------------------------
def plot_boxcox_figs_feature(
    rad_data, map_name, tumours, features, save_path=None):
    '''
    Plot treatment_tumours for all features a bar chart with error bars
    '''
    features = ['original_' + f for f in features]
    feature_labels = get_feature_labels(features)[0]

    #Get tumour data for medians
    rad_tumour_fun = dr.select_tumour_fun(tumours)

    for i_f,feature in enumerate(features):
        feature_color = get_feature_colors([feature])
        group_label = feature.split('_')[1].upper()

        if group_label == 'SHAPE':
            group_label = 'Shape'
        elif group_label == 'FIRSTORDER':
            group_label = 'First-order'

        v1234_data = rad_tumour_fun(rad_data, map_name, feature, [1,2,3,4])
        v1234_data_bc = np.copy(v1234_data)
        valid = np.isfinite(v1234_data) & np.isfinite(v1234_data_bc)
        v12 = np.mean(v1234_data[:,0:2], axis = 1)
        
        #Apply box-cox transform
        lmbda = dr.boxcox_lambda(v1234_data_bc)
        dr.boxcox_transform(v1234_data_bc, lmbda)
        v12_bc = np.mean(v1234_data_bc[:,0:2], axis = 1)

        #Check if normal before/after tramsform
        sh_p = shapiro(v12[np.isfinite(v12)])[1]
        sh_p_bc = shapiro(v12_bc[np.isfinite(v12_bc)])[1]
        sh = int(sh_p < 0.05)
        sh_bc = int(sh_p_bc < 0.05)

        #Set up figure
        plt.figure(figsize=(20,4))

        #1st column is hist of original feature valuess
        plt.subplot(1,5,1)
        plt.hist(v1234_data[valid], bins = 20, color = feature_color, density=False)
        plt.xlabel(f'{group_label} {feature_labels[i_f]}')
        plt.title('Original distribution')
        plt.yticks(fontsize = 9)

        #2nd column is prob-plot vs normal for original
        ax2 = plt.subplot(1,5,2)
        probplot(v1234_data[valid], plot = ax2, rvalue=True)
        ax2.set_ylabel('')
        plt.yticks(fontsize = 9)

        #3rd column is the Box-cox plot - shift the original data to make this work
        v1234_data += (1e-6 - v1234_data[valid].min())
        ax3 = plt.subplot(1,5,3)
        boxcox_normplot(v1234_data[valid], -20, 20, plot = ax3)
        maxlog = boxcox(v1234_data[valid])[1]
        ax3.axvline(maxlog, color='r')
        ax3.set_ylabel('')
        plt.yticks(fontsize = 9)

        #1st column is hist of Box-Cox transformed feature valuess
        plt.subplot(1,5,4)
        plt.hist(v1234_data_bc[valid], bins = 20, color = feature_color, density=False)
        plt.xlabel(f'Box-Cox {group_label} {feature_labels[i_f]}')
        plt.title('Transformed distribution')
        plt.yticks(fontsize = 9)

        #2nd column is prob-plot vs normal for Box-Cox transformed
        ax5 = plt.subplot(1,5,5)
        probplot(v1234_data_bc[valid], plot = ax5)
        ax5.set_ylabel('')
        plt.yticks(fontsize = 9)

        if save_path:
            save_file = f'{save_path}_{group_label}_{feature_labels[i_f]}_{sh}{sh_bc}.png'
            plt.savefig(save_file, bbox_inches='tight', facecolor='w')
        else:
            plt.show()

        if i_f:
            plt.close()
            