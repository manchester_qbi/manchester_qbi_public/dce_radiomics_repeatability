'''
This script contains experimental work that did not make it into the main paper.
It can be used for experimenting with new methods/plots, but nothing in this
script should be considered permanent or validated. Any analysis intended to be
kept should be moved to the main dce_radiomics_interactive_script.py
'''
#%%
%load_ext autoreload

%autoreload 2

%config Completer.use_jedi = False
#%%
import os

import dce_radiomics_stats
import dce_radiomics_plots
import dce_radiomics as dr
import make_map_plots
import pickle

#%%

#%%
n_boots = dr.n_boots
for visit in [3, 4]:
    dr.compute_features_correlation('combined', visit = visit, n_boots = n_boots)

dr.compute_icc('combined', n_boots = n_boots)
dr.compute_treatment_effect('combined', treatment_visit=3, n_boots = n_boots)
dr.compute_treatment_effect('combined', treatment_visit=4, n_boots = n_boots)

dr.make_boot_means('combined')
#%%
dr.make_icc_plot('combined', map_names = ['Ktrans'], bcs=[True], boot_str='_boot_mean')

visit_labels = ['Avg baseline', '48 hours', 'EC1']#, '21 days'
for visit in [3,4]:
    dr.make_treatment_effect_plot(
        'combined', 
        map_names = ['Ktrans'],
        treatment_visit=visit, 
        treatment_label=visit_labels[visit-2], 
        boot_str='_boot_mean')

for visit,label in zip([1, 3, 4],visit_labels):
    make_correlation_plot('combined', visit=visit, treatment_label=label, 
        boot_str='_boot_mean')
#%%
for visit,label in zip([1, 3, 4],visit_labels):
    dr.make_median_correlation_plot('combined', 
        visit=visit, 
        map_names = ['Ktrans'],
        treatment_label=label, 
        boot_str='_boot')

dr.make_checklist_map('combined', map_names = ['Ktrans'], boot_str='_boot_mean')

#%%
study_name = 'travastin'
rad_data = dr.load_rad_data(study_name)
map_names = ['Ktrans', 'IAUC60', 'v_e', 'v_p']
xls_path = os.path.join('results', f'{study_name}_median.xlsx')
features = [
    'original_firstorder_Median'
]
dce_radiomics_stats.rad_data_to_excel(
    dr.patients[study_name], rad_data, map_names, [1,2,3,4], features, xls_path)

