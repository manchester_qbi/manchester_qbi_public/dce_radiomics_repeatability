import os
import glob

import matplotlib.pyplot as plt
import numpy as np

#import nibabel as nb

from QbiPy.dce_models import tissue_concentration, tofts_model, data_io, dce_aif
from QbiPy.image_io import xtr_files, analyze_format


#-------------------------------------------------------
def guess_injection_image(dyn_concentrations, dyn_times, 
    iauc_val, iauc_time, debug):

    min_t = 7
    max_t = 11
    errs = np.zeros(max_t-min_t+1)
    for i_t in range(min_t,max_t+1):
        iauc_n = tissue_concentration.compute_IAUC(
            dyn_concentrations, dyn_times, i_t, iauc_time)

        errs[i_t-min_t] = np.nanmedian(np.abs(iauc_n - iauc_val))

    injection_image = np.argmin(errs)+min_t
    if debug:
        iauc_n = tissue_concentration.compute_IAUC(
            dyn_concentrations, dyn_times, injection_image, iauc_time);
        
        max_val = np.max(iauc_val)
        plt.figure(figsize=(12,6))
        plt.subplot(1,2,1)
        plt.plot(np.arange(min_t, max_t+1), errs)
        plt.xlabel('Volume number')
        plt.ylabel('Mean absolute difference IAUC_{60}')
        plt.subplot(1,2,2)
        plt.plot(iauc_val, iauc_n, 'rx')
        plt.plot([0, max_val], [0, max_val], 'k--')
        plt.xlabel('Madym $IAUC_{60}$')
        plt.ylabel('New $IAUC_{60}$')
        plt.axis([0,max_val,0,max_val], 'equal')

    return injection_image

def match_injection_image(visit_dir, n_vols, IAUC_t, IAUC_path, T1_path, M0_path, 
    relax_coeff = 3.4, debug = False, orig_scaling = 100):
    ''' '''

    #Load in the existing IAUC60, 90 and 120 so we can check we achieve the
    #same values, use IAUC60 to generate an enhancing mask

    iauc = analyze_format.read_analyze_img(
        os.path.join(visit_dir, IAUC_path)) / orig_scaling
    enhancing_mask = iauc > 0

    iauc_vals = iauc[enhancing_mask]

    #Load in the dynamic signals and dynamic times
    dynamic_path = os.path.join(visit_dir, 'dynamic', 'dyn_')

    dyn_signals = data_io.get_dyn_vals(dynamic_path, n_vols, enhancing_mask)
    timestamps, dyn_TR, dyn_FA, _ = data_io.get_dyn_xtr_data(dynamic_path, n_vols)

    FA = dyn_FA[0]
    TR = dyn_TR[0]

    dyn_times = [xtr_files.timestamp_to_secs(t) for t in timestamps]
    dyn_times -= dyn_times[0]

    #Load in T1 and M0 and convert signals to concentrations
    T1 = analyze_format.read_analyze_img(os.path.join(visit_dir, T1_path))
    M0 = analyze_format.read_analyze_img(os.path.join(visit_dir, M0_path))
    T1_vals = T1[enhancing_mask]
    M0_vals = M0[enhancing_mask]

    dyn_concentrations = tissue_concentration.signal_to_concentration(
        dyn_signals, T1_vals, M0_vals, FA, TR, relax_coeff, 0)

    #Workout the injection image
    injection_image = guess_injection_image(
        dyn_concentrations, dyn_times, iauc_vals, IAUC_t, debug)

    #Compute IAUC, first at 60, 90, 120
    iauc_n = tissue_concentration.compute_IAUC(
        dyn_concentrations, dyn_times, injection_image, IAUC_t)

    err = np.nanmean(np.abs(iauc_n - iauc_vals))
    print(f'Mean absolute difference for {visit_dir}, IAUC60 = {err:3.2f}')
    return injection_image, err

def match_aif(visit_dir, n_vols, analysis_dir, T1_path, M0_path, 
    relax_coeff = 3.4, orig_scaling = 100, debug = False):
    
    #Get list of AIFs
    aifs = glob.glob(os.path.join(visit_dir, 'slice*AIF.txt')) 

    #Load Ktrans, Ve, Vp and tau_a
    Ktrans = analyze_format.read_analyze_img(
        os.path.join(analysis_dir, 'Ktrans.raw.hdr')) / orig_scaling
    v_e = analyze_format.read_analyze_img(
        os.path.join(analysis_dir, 'Ve.raw.hdr')) / orig_scaling
    v_p = analyze_format.read_analyze_img(
        os.path.join(analysis_dir, 'Vp.raw.hdr')) / orig_scaling
    tau_a = analyze_format.read_analyze_img(
        os.path.join(analysis_dir, 'tau.raw.hdr')) / orig_scaling

    enhancing_mask = Ktrans > 0

    Ktrans = Ktrans[enhancing_mask]
    v_e = v_e[enhancing_mask]
    v_p = v_p[enhancing_mask]
    tau_a = tau_a[enhancing_mask]

    #Load in the dynamic signals
    dynamic_path = os.path.join(visit_dir, 'dynamic', 'dyn_')
    dyn_signals = data_io.get_dyn_vals(dynamic_path, n_vols, enhancing_mask)
    _, dyn_TR, dyn_FA, _ = data_io.get_dyn_xtr_data(dynamic_path, 1)

    FA = dyn_FA[0]
    TR = dyn_TR[0]

    #Load in T1 and M0 and convert signals to concentrations
    T1 = analyze_format.read_analyze_img(os.path.join(visit_dir, T1_path))
    M0 = analyze_format.read_analyze_img(os.path.join(visit_dir, M0_path))
    T1_vals = T1[enhancing_mask]
    M0_vals = M0[enhancing_mask]

    C_t = tissue_concentration.signal_to_concentration(
        dyn_signals, T1_vals, M0_vals, FA, TR, relax_coeff, 0)

    aif_sses = np.zeros(len(aifs))

    for i_aif, aif_path in enumerate(aifs):
        #Create AIF object from file
        aif = dce_aif.Aif(aif_type = dce_aif.AifType.FILE, filename=aif_path)

        C_t_model = tofts_model.concentration_from_model(
            aif, Ktrans, v_e, v_p, tau_a
        )

        sse = np.nansum((C_t - C_t_model)**2, axis=1)

        if debug:
            plt.figure(figsize=(12,8))
            for i in range(12):
                plt.subplot(3,4,i+1)
                plt.plot(C_t[i,:], 'r.')
                plt.plot(C_t_model[i,:], 'b-')
                plt.title(f'SSE = {sse[i]:4.3f}')
        
        aif_sses[i_aif] = np.nanmedian(sse)
    
    min_sse = np.min(aif_sses)
    aif_idx = np.argmin(aif_sses)
    aif_file = os.path.basename(aifs[aif_idx])
    aif = int(aif_file.split('_')[1])
    return aif, min_sse
