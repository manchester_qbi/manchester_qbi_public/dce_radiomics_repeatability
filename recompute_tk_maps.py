#%%
%load_ext autoreload

%autoreload 2

%config Completer.use_jedi = False


import os
import shutil
import pickle

import nibabel as nb
import numpy as np

from QbiMadym import madym_DCE, madym_T1, utils
import projects.patient_data as patient_data

import match_options

audit_dir = 'C:/isbe/qbi/data/travastin/dce_radiomics/mdm_reanalysis/'
dummy_run = False
#%%
#-------------------------------------------------------------
def map_t1(visit_dir, T1_vols = ['FA_1_mean.hdr', 'FA_2_mean.hdr', 'FA_3_mean.hdr']):
    print(f'Mapping T1 for {visit_dir}')
    
    if not os.path.exists(os.path.join(visit_dir, T1_vols[0])):
        return

    madym_T1.run(
        output_dir = 'mdm_analysis/T1_mapping',
        working_directory = visit_dir,
        T1_vols = T1_vols, 
        method = 'VFA',
        noise_thresh = 10,
        img_fmt_r = 'ANALYZE',
        img_fmt_w = 'ANALYZE',
        return_maps = True,
        audit_dir = audit_dir,
        overwrite = False,
        dummy_run = dummy_run)

#-------------------------------------------------------------
def fit_ETM(visit_dir, tumour, tumour_num):
    if not tumour:
        #Don't print out 'Missing...' here - the tumour isn;t missing, it never existed!
        return

    if type(tumour.parameters_['Ktrans']) == str:
        print(f'Tumour {tumour_num} not valid, skipping')
        return

    if os.path.exists(os.path.join(
        visit_dir, 'mdm_analysis', f'ETM_tumour{tumour_num}', 'Ktrans.nii.gz')):
        return

    print('*'*80)
    print(f'Fitting ETM to tumour {tumour_num}')

    FA_dir = os.path.join(visit_dir, 'FA_1')
    if tumour_num == 1 and os.path.exists(os.path.join(FA_dir, 'tumour')):
        tumour_dir = os.path.join(FA_dir, 'tumour')
    else:
        tumour_dir = os.path.join(FA_dir, 'tumour'+str(tumour_num))

    #Check whether we use an auto-generated or assumed AIF
    if 'auto' in tumour.parameters_['DCE_Source'] and \
        type(tumour.parameters_['AIF_slice']) is int and \
        os.path.isfile(os.path.join(visit_dir, 
            'slice_' + str(tumour.parameters_['AIF_slice']) + '_Auto_AIF.txt')):
        
        aif_path = ('slice_{aif_slice}_Auto_AIF.txt'.format(
                aif_slice =tumour.parameters_['AIF_slice']))
        hct = 0.0
        analysis_dir = os.path.join(tumour_dir, 'Tofts_plus_vp_Auto_AIF_results')
    else:
        aif_path = None
        hct = 0.42
        analysis_dir = os.path.join(tumour_dir, 'Tofts_plus_vp_assumed_AIF_results')

    roi_path = os.path.join(analysis_dir, 'ROI.raw.hdr')
    injection_image = tumour.parameters_['Injection_image']

    tumour_output_dir = 'mdm_analysis/ETM_tumour' + str(tumour_num)

    madym_DCE.run(
        model='ETM', 
        working_directory = visit_dir,
        output_dir = tumour_output_dir,
        dynamic_basename = 'dynamic/dyn_',
        input_Ct = False,
        T1_name = 'mdm_analysis/T1_mapping/T1.hdr',
        M0_ratio = True,
        injection_image = injection_image,
        hct = hct,
        r1_const = 3.4,
        roi_name = roi_path,
        aif_name = aif_path,
        opt_type = 'BLEIC',
        test_enhancement = True,
        img_fmt_r = 'ANALYZE',
        img_fmt_w = 'NIFTI_GZ',
        voxel_size_warn_only = True,
        overwrite = True,
        audit_dir = audit_dir,
        dummy_run = dummy_run)

#%%
def copy_param_maps(visit_dir, tumour, tumour_num):

    share_root = 'X:\\projects2\\travastin'
    analysis_dir = os.path.join('mdm_analysis', f'ETM_tumour{tumour_num}')
    
    if not tumour:
        #Don't print out 'Missing...' here - the tumour isn;t missing, it never existed!
        return

    if type(tumour.parameters_['Ktrans']) == str:
        print(f'Tumour {tumour_num} not valid, skipping')
        return

    subject_dir, v = os.path.split(visit_dir)
    study_dir, s = os.path.split(subject_dir)
    old_path = os.path.join(visit_dir, analysis_dir)
    new_path = os.path.join(share_root, s, v, analysis_dir)

    if os.path.exists(os.path.join(new_path, 'Ktrans.nii.gz')):
        return

    try:
    
        #shutil.copytree(old_path, new_path, dirs_)
        src_files = os.listdir(old_path)
        for file_name in src_files:
            full_file_name = os.path.join(old_path, file_name)
            if os.path.isfile(full_file_name):
                shutil.copy(full_file_name, new_path)
        print(f'Copied maps for {visit_dir}, tumour {tumour_num}')

    except:
        print(f'Copy falied for {visit_dir}, tumour {tumour_num}')
#%%
def check_params_present(visit_dir, tumour, tumour_num, valid_list, missing_list):
    if not tumour:
        #Don't print out 'Missing...' here - the tumour isn;t missing, it never existed!
        return

    if type(tumour.parameters_['Ktrans']) == str:
        return

    analysis_dir = os.path.join(visit_dir, 'mdm_analysis', f'ETM_tumour{tumour_num}')

    valid = os.path.exists(os.path.join(analysis_dir, 'IAUC60.nii.gz')) \
        and  os.path.exists(os.path.join(analysis_dir, 'Ktrans.nii.gz')) \
            and os.path.exists(os.path.join(analysis_dir, 'v_e.nii.gz')) \
                and os.path.exists(os.path.join(analysis_dir, 'v_p.nii.gz'))

    if valid:
        valid_list.append((visit_dir,tumour_num))
    else:
        missing_list.append((visit_dir,tumour_num))

#%%
def correct_map(analysis_dir, map_name, bad_mask):
    orig_map = nb.load(os.path.join(analysis_dir, f'{map_name}.nii.gz'))
    map_data = 100*orig_map.get_fdata().copy()

    bad_mask_map = bad_mask | ~np.isfinite(map_data)
    map_data[bad_mask_map] = 0
    new_map = nb.Nifti1Image(map_data, orig_map.affine, orig_map.header)
    nb.save(new_map, os.path.join(analysis_dir, f'{map_name}_radiomics.nii.gz'))

def correct_maps(visit_dir, tumour, tumour_num):
    if not tumour:
        #Don't print out 'Missing...' here - the tumour isn;t missing, it never existed!
        return

    if type(tumour.parameters_['Ktrans']) == str:
        return

    print(f'Correcting {visit_dir}, tumour {tumour_num}')
    analysis_dir = os.path.join(visit_dir, 'mdm_analysis', f'ETM_tumour{tumour_num}')

    IAUC60_path = os.path.join(analysis_dir, 'IAUC60.nii.gz')
    if not os.path.exists(IAUC60_path):
        return
    IAUC60 = nb.load(IAUC60_path)

    bad_mask = IAUC60.get_fdata() < 0

    for map_name in ['IAUC60', 'Ktrans', 'v_e', 'v_p', 'enhVox']:
        correct_map(analysis_dir, map_name, bad_mask)

#%%
# Match injection image
def match_injection_image(visit_dir, tumour, tumour_num, debug = False, orig_scaling = 100):

    if not tumour:
        #Don't print out 'Missing...' here - the tumour isn;t missing, it never existed!
        return None

    if type(tumour.parameters_['Ktrans']) == str:
        print(f'Tumour {tumour_num} not valid, skipping')
        return None

    if not os.path.exists(os.path.join(
        visit_dir, 'mdm_analysis', 'T1_mapping', 'T1.hdr')):
        return None

    FA_dir = os.path.join(visit_dir, 'FA_1')
    if tumour_num == 1 and os.path.exists(os.path.join(FA_dir, 'tumour')):
        tumour_dir = os.path.join(FA_dir, 'tumour')
    else:
        tumour_dir = os.path.join(FA_dir, 'tumour'+str(tumour_num))

    analysis_dir = os.path.join(tumour_dir, 'Tofts_plus_vp_assumed_AIF_results')

    IAUC_path = os.path.join(analysis_dir, 'IAUC60.raw.hdr')
    T1_path = os.path.join(visit_dir, 'mdm_analysis', 'T1_mapping', 'T1.hdr')
    M0_path = os.path.join(visit_dir, 'mdm_analysis', 'T1_mapping', 'M0.hdr')

    inj_img, err = match_options.match_injection_image(
        visit_dir, 75, 60, IAUC_path, T1_path, M0_path, 
        relax_coeff = 3.4, debug = debug, orig_scaling = orig_scaling)
    tumour.parameters_['Injection_image'] = inj_img
    tumour.parameters_['Injection_image_warning'] = err

    print(f'Setting {visit_dir}, tumour {tumour_num} inj = {inj_img}')
    if err > 10:
        print(f'WARNING: diff = {err}')
    return inj_img

#%%    
#Now we need to try to workout which AIF was used for the auto cases
def match_aif(visit_dir, tumour, tumour_num, debug = False):
    if not tumour:
        #Don't print out 'Missing...' here - the tumour isn;t missing, it never existed!
        return None

    if type(tumour.parameters_['Ktrans']) == str:
        print(f'Tumour {tumour_num} not valid, skipping')
        return None

    if not os.path.exists(os.path.join(
        visit_dir, 'mdm_analysis', 'T1_mapping', 'T1.hdr')):
        return None

    FA_dir = os.path.join(visit_dir, 'FA_1')
    if tumour_num == 1 and os.path.exists(os.path.join(FA_dir, 'tumour')):
        tumour_dir = os.path.join(FA_dir, 'tumour')
    else:
        tumour_dir = os.path.join(FA_dir, 'tumour'+str(tumour_num))

    analysis_dir = os.path.join(tumour_dir, 'Tofts_plus_vp_Auto_AIF_results')
    T1_path = os.path.join(visit_dir, 'mdm_analysis', 'T1_mapping', 'T1.hdr')
    M0_path = os.path.join(visit_dir, 'mdm_analysis', 'T1_mapping', 'M0.hdr')

    aif, sse = match_options.match_aif(
        visit_dir, 75, analysis_dir, T1_path, M0_path, 
        relax_coeff = 3.4, orig_scaling = 100, debug = debug)
    
    tumour.parameters_['AIF_slice'] = aif
    tumour.parameters_['AIF_slice_warning'] = sse

    print(f'Setting {visit_dir}, tumour {tumour_num} AIF = {aif}') 
    return aif  

#%%
#-------------------------------------------------------------
#utils.set_madym_root(
#    'C:\\isbe\\code\\manchester_qbi\\public\\madym_cxx_builds\\msvc2019\\bin\\Release')

#Load Travastin patients
study_dir = 'V:/isbe/qbi/data/travastin/'
xls_path = 'travastin1_summaryData_qcLevel4_10-Dec-2015-162730 - MB.xlsx'
patients = patient_data.load_data_from_xls(study_dir, xls_path)
patients.pop(-1)
print('%d patients loaded' % len(patients))
visits = [1, 2, 3, 4, 5]

#%%
patients_file = os.path.join('results', 'travastin_patients.pkl')

with open(patients_file, 'wb') as output:
    pickle.dump(patients, output, pickle.HIGHEST_PROTOCOL)

#%%
for patient in patients:
    patient_id = patient.id_
    patient_dir = os.path.join(study_dir, patient_id)

    for visit in visits:
        visit_dir = os.path.join(patient_dir, 'visit'+str(visit))
        print('Visit dir: ', visit_dir)

        map_t1(visit_dir)

        for tumour_num, tumour in enumerate(patient.tumours_[visit-1], start=1):
            fit_ETM(visit_dir, tumour, tumour_num)

#%%
study_dir = 'V:\\isbe\\qbi\\data\\travastin'

for patient in patients:
    patient_id = patient.id_
    patient_dir = os.path.join(study_dir, patient_id)

    for visit in visits:
        visit_dir = os.path.join(patient_dir, 'visit'+str(visit))

        for tumour_num, tumour in enumerate(patient.tumours_[visit-1], start=1):
            copy_param_maps(visit_dir, tumour, tumour_num)

#%%
study_dir = 'X:\\projects2\\travastin'
missing_list = []
valid_list = []
for patient in patients:
    patient_id = patient.id_
    patient_dir = os.path.join(study_dir, patient_id)

    for visit in visits:
        visit_dir = os.path.join(patient_dir, 'visit'+str(visit))
        
        for tumour_num, tumour in enumerate(patient.tumours_[visit-1], start=1):
            check_params_present(visit_dir, tumour, tumour_num, valid_list, missing_list)

#%%
#%%
study_dir = 'X:\\projects2\\travastin'

for patient in patients:
    patient_id = patient.id_
    patient_dir = os.path.join(study_dir, patient_id)

    for visit in visits:
        visit_dir = os.path.join(patient_dir, 'visit'+str(visit))

        for tumour_num, tumour in enumerate(patient.tumours_[visit-1], start=1):
            correct_maps(visit_dir, tumour, tumour_num)

#%%
study_dir = 'X:/data/bevacizumab/'
xls_path = 'results_spreadsheet_bevacizumab_postregistration_analysis_Tofts_plus_vp_Auto_AIF_results_26_Jul_2010.xlsx'
patients = patient_data.load_data_from_xls(study_dir, xls_path, max_visits = 6, max_tumours= 10)
patients.pop(-1)
print('%d patients loaded' % len(patients)) 

visits = [1, 2, 4, 6]
#%%
for patient in patients:
    patient_dir = os.path.join(study_dir, patient.id_)

    for visit in visits:
        visit_dir = os.path.join(patient_dir, 'visit'+str(visit))
        try:
            map_t1(visit_dir, T1_vols = ['FA_2deg.hdr', 'FA_10deg.hdr', 'FA_20deg.hdr', 'FA_30deg.hdr'])
        except:
            print(f'Failed T1 mapping for {visit_dir}')
#%%

#%%
for patient in patients:
    patient_id = patient.id_
    patient_dir = os.path.join(study_dir, patient_id)

    for visit in visits:
        visit_dir = os.path.join(patient_dir, 'visit'+str(visit))

        inj = None
        for tumour_num, tumour in enumerate(patient.tumours_[visit-1], start=1):
            if tumour is None:
                continue
            
            if inj is None:
                try:
                    inj = tumour.parameters_['Injection_image']
                except:
                    inj = match_injection_image(
                        visit_dir, tumour, tumour_num, debug=True, orig_scaling=100)

            else:
                tumour.parameters_['Injection_image'] = inj
            
            
# %%
for patient in patients:
    patient_id = patient.id_
    patient_dir = os.path.join(study_dir, patient_id)

    for visit in visits:
        visit_dir = os.path.join(patient_dir, 'visit'+str(visit))
        aif = None
        for tumour_num, tumour in enumerate(patient.tumours_[visit-1], start=1):
            if tumour is None:
                continue
            if aif is None:
                try:
                    aif = tumour.parameters_['AIF_slice']
                except:
                    aif = match_aif(visit_dir, tumour, tumour_num, debug=False)
            else:
                tumour.parameters_['AIF_slice'] = aif

# %%
for patient in patients:
    patient_id = patient.id_
    patient_dir = os.path.join(study_dir, patient_id)

    for visit in visits:
        visit_dir = os.path.join(patient_dir, 'visit'+str(visit))
        for tumour_num, tumour in enumerate(patient.tumours_[visit-1], start=1):
            if tumour is not None:
                tumour.parameters_['DCE_Source'] = 'autoAIF'

patients_file = os.path.join('results', 'bevacizumab_patients.pkl')
with open(patients_file, 'wb') as output:
    pickle.dump(patients, output, pickle.HIGHEST_PROTOCOL)

# %%
for patient in patients[8:9]:
    patient_id = patient.id_
    patient_dir = os.path.join(study_dir, patient_id)

    for visit in visits:
        visit_dir = os.path.join(patient_dir, 'visit'+str(visit))
        for tumour_num, tumour in enumerate(patient.tumours_[visit-1], start=1):
            fit_ETM(visit_dir, tumour, tumour_num)

# %%
valid_list = []
missing_list = []
for patient in patients:
    patient_id = patient.id_
    patient_dir = os.path.join(study_dir, patient_id)

    for visit in visits:
        visit_dir = os.path.join(patient_dir, 'visit'+str(visit))
        for tumour_num, tumour in enumerate(patient.tumours_[visit-1], start=1):
            check_params_present(visit_dir, tumour, tumour_num, valid_list, missing_list)
# %%
for patient in patients:
    patient_id = patient.id_
    patient_dir = os.path.join(study_dir, patient_id)

    for visit in visits:
        visit_dir = os.path.join(patient_dir, 'visit'+str(visit))
        for tumour_num, tumour in enumerate(patient.tumours_[visit-1], start=1):
            correct_maps(visit_dir, tumour, tumour_num)
# %%
