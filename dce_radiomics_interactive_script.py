''' 
This is the main script for producing results for our paper 
*Radiomic features derived from DCE-MRI Ktrans maps offer marginal benefit over analysis with median values* submitted 
to European Radiology, with manuscript number EURA-D-23-00567.

It is designed to run interactively in an iPython kernel using the python interpreter
defined in the `dce_radiomics` conda environment. This environment can be
created using the included conda set-up file `conda_env.yml` by calling

    > conda env create -f conda_env.yml

This script performs the following:

    - Compute radiomics features for each tumour in the two study datasets

    - Computes summary stats epxorted to CSV spreadsheets

    - Computes each of the statistical metrics described in the methods section of the main paper for the radiomics features

    - Produces plots of the statistical metrics used in the figures of the main paper
'''
#%%
%load_ext autoreload

%autoreload 2

%config Completer.use_jedi = False
#%%
import os
import dce_radiomics as dr
#%%
#---------------------------------------------------------------
# Make folders for data and results
os.makedirs('data', exist_ok=True)
os.makedirs('results', exist_ok=True)
#%%
#---------------------------------------------------------------
# Compute radiomics features for tumours in both studies
for study_name in ['travastin', 'bevacizumab']:
    dr.compute_radiomics(study_name)

#%%
#---------------------------------------------------------------
dr.make_tumour_counts_table('travastin')
dr.make_tumour_counts_table('bevacizumab')
#%%
#---------------------------------------------------------------
# Make summary stats spreadhseets
for bc in [True, False]:
    dr.make_summary_xls('travastin', boxcox = bc)
    dr.make_summary_xls('combined', boxcox = bc)
#%%
#---------------------------------------------------------------
# Compute data structures for the statistical analysis
# 1. ICC
# 2. Treatment effects at cohort level
# 3. Treat ment effect for individual tumours
# 4. 
dr.compute_icc('combined')
dr.compute_wCV('combined')
for visit in [1, 3, 4]:
    dr.compute_features_correlation('combined', visit=visit)

    if visit > 1:
        dr.compute_treatment_effect('combined', 
            treatment_visit=visit, boxcox=True)
        dr.compute_treatment_tumours('combined', 
            treatment_visit=visit, boxcox=True)

#%%
#---------------------------------------------------------------
# Make plots 
#    
map_names = ['Ktrans']#, 'IAUC60']
bc = True
#%%
dr.make_icc_plot('combined', map_names = map_names)
dr.make_wCV_plot('combined', map_names = map_names)
#%%
for feature_type in ['median', 'volume']:
    for visit in [3,4]:
        dr.make_treatment_effect_plot(
            'combined',
            map_names = map_names, 
            boxcox = bc,
            treatment_visit=visit, 
            treatment_label=None,
            feature_type = feature_type)

#%
for feature_type in ['median', 'volume']:
    for visit in [3,4]:
        dr.make_treatment_tumours_plot(
            'combined', 
            map_names = map_names,
            boxcox = bc,
            treatment_visit=visit, 
            treatment_label=None,
            feature_type = feature_type)

#%
for feature_type in ['median', 'volume']:
    for visit in [3, 4]:
        dr.make_median_correlation_plot('combined', 
            visit=visit, treatment_label=None,
            map_names = map_names,
            boxcox = bc, feature_type = feature_type)
        continue
        dr.make_correlation_plot('combined', 
            visit=visit, treatment_label=label,
            map_names = map_names,
            boxcox = bc)
#%%
visit_labels = ['avg baseline', '48 hrs', 'EC1']
for visit in [3,4]:
    dr.make_treatment_features_plot(
        'combined', 
        map_names = map_names,
        boxcox = bc,
        treatment_visit=visit, 
        treatment_label=visit_labels[visit-2])


#%%
dr.make_ranking_map('combined', map_names, boxcox = bc)
#%%
dr.make_median_summary_plots(map_names = map_names, apply_boxcox=bc)
dr.make_median_summary_plots(map_names = map_names, apply_boxcox=bc, 
    feature_type = 'volume', units = ['$mm^3$'])     
#%%
dr.make_waterfall_plot('combined', map_names = map_names, apply_boxcox=bc)
dr.make_waterfall_plot('combined', map_names = map_names, apply_boxcox=bc,
    feature_type = 'volume')
# %%
dr.make_features_lookup_table()
# %%
dr.make_waterfall_plot('combined', map_names = ['Ktrans'], apply_boxcox=False)
dr.make_waterfall_plot('combined', map_names = ['Ktrans'], apply_boxcox=True)
#%%
for study in ['travastin', 'bevacizumab']
    dr.make_tumour_maps(study, map_names)
