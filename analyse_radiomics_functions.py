#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Module containing functions used in analysing radiomics output.

Created on Wed Feb 5 09:15:46 2020

@author: damienmchugh
"""
import numpy as np
from scipy import stats

#-----------------------------------------------------------------------------            
def icc(repeat_measurements, icc_type, conf_level = 95):
    """Calculates intraclass correlation for repeated measurements.
    
    This replaces the depreciated function above, as it includes confidence
    intervals as well as ICC. Code is based on the icc function in the R
    package irr.
    
    References:
        McGraw & Wong, Psychol Methods 1996;1:30-46
        Weir, J Strength Cond Res 2005;19:231-240
        Gamer et al., R irr package, v.0.84.1 (https://rdrr.io/cran/irr/)
        Liljequist et al., PLoS ONE 2019, 14:7e0219854
    
    Calculation here is ICC(2,1), corresponding to:
        icc(rep_meas,model="twoway",type="agreement",unit="single") in R
    Updated to include ICC(1) and ICC(2,1) consistency 
    Notation here matches Weir 2005:
        n - number of subjects
        k - number of trials
        SStotal - total sum of squares
        MSs - subjects mean square
        MSw - within-subjects mean square
        MSt - trials mean square
        MSe - error mean square
    
    Inputs:
        repeat_measurement - n x 2 array of pair-wise measurements
        icc_type - type of ICC to calculate
        conf_level - optional argument specifying confidence level (%) for CIs
                   - defaults to 95% CIs
    Outputs:
        icc - intraclass correlation coefficient for model specified by icc_type
        cis - lower and upper confidence intervals for ICC
        p_value - p-value for test that ICC is greater than 0
        F_value - F statistic associated with pval test
    """
    # Analysis of variance calculations
    [n, k] = np.shape(repeat_measurements)
    SStotal = np.var(repeat_measurements.flatten(), ddof = 1)*(n*k - 1)
    MSs = np.var(np.mean(repeat_measurements,1), ddof = 1)*k
    MSw = np.sum(np.var(repeat_measurements, axis = 1, ddof = 1)/n)
    MSt = np.var(np.mean(repeat_measurements,0), ddof = 1)*n
    MSe = (SStotal - MSs*(n - 1) - MSt*(k - 1))/((n - 1)*(k - 1))
    
    # ICC calculations
    if icc_type == 'icc_1_1':
        # ICC(1,1) (Weir, Table 5; equivalent to McGraw & Wong, Table 4)
        # Also called ICC(1) in Liljequist et al.
        icc = (MSs - MSw)/(MSs + (k-1)*MSw)
        
        # Test statistics (McGraw & Wong, Table 8)
        r0 = 0 # Null hypothesis that icc is 0
        F_value = (MSs/MSw)*( (1 - r0)/(1 + (k - 1)*r0) )
        df1 = n - 1
        df2 = n*(k - 1)
        p_value = 1 - stats.f.cdf(F_value, df1, df2)
        
        # Confidence intervals (McGraw & Wong, Table 7)
        Fobs = MSs/MSw
        alpha = 1 - conf_level/100
        Ftab_l = stats.f.ppf(1 - alpha/2, n - 1, n*(k - 1))
        Ftab_u = stats.f.ppf(1 - alpha/2, n*(k - 1), n - 1)
        FL = Fobs/Ftab_l
        FU = Fobs*Ftab_u
        lbound = (FL - 1)/(FL + (k - 1))
        ubound = (FU - 1)/(FU + (k - 1))
        cis = [lbound, ubound]
    elif icc_type == 'icc_2_1':
        # ICC(2,1) (Weir, Table 5; equivalent to McGraw & Wong, Table 4)
        # Also called ICC(A,1) in Liljequist et al.
        icc = (MSs - MSe)/(MSs + (k - 1)*MSe + k*(MSt - MSe)/n)
        
        # Test statistics (McGraw & Wong, Table 8)
        r0 = 0 # Null hypothesis that icc is 0
        a = (k*r0)/(n*(1 - r0))
        b = 1 + (k*r0*(n - 1))/(n*(1 - r0))
        F_value = MSs/(a*MSt + b*MSe)
        
        # Possible bug in R irr? - p-val calc should use a and b with r0 = 0?
        #a = (k*icc)/(n*(1 - icc))
        #b = 1 + (k*icc*(n - 1))/(n*(1 - icc))
        v = (a*MSt + b*MSe)**2/((a*MSt)**2/(k - 1) + (b*MSe)**2/((n - 1)*(k - 1)))
        df1 = n-1
        df2 = v
        p_value = 1 - stats.f.cdf(F_value, df1, df2)
        
        # Confidence intervals (McGraw & Wong, Table 7)
        alpha = 1 - conf_level/100
        a = (k*icc)/(n*(1 - icc))
        b = 1 + (k*icc*(n - 1))/(n*(1 - icc))
        v = (a*MSt + b*MSe)**2/((a*MSt)**2/(k - 1) + (b*MSe)**2/((n - 1)*(k - 1)))
        FL = stats.f.ppf(1 - alpha/2, n - 1, v)
        FU = stats.f.ppf(1 - alpha/2, v, n - 1)
        lbound = (n*(MSs - FL*MSe))/(FL*(k*MSt + (k*n - k - n)*MSe) + n*MSs)
        ubound = (n*(FU*MSs - MSe))/(k*MSt + (k*n - k - n)*MSe + n*FU*MSs)
        cis = [lbound, ubound]
    elif icc_type == 'icc_2_1_c':
        # Consistency rather than absolute agreement
        # Called ICC(C,1) in Liljequist et al.
        icc = (MSs - MSe)/(MSs + (k - 1)*MSe)
        
        # Test statistics (McGraw & Wong, Table 8)
        r0 = 0 # Null hypothesis that icc is 0
        F_value = (MSs/MSe)*( (1 - r0)/(1 + (k - 1)*r0) )
        df1 = n - 1
        df2 = (n -1 )*(k - 1)
        p_value = 1 - stats.f.cdf(F_value, df1, df2)
        
        # Confidence intervals (McGraw & Wong, Table 7)
        Fobs = MSs/MSe
        alpha = 1 - conf_level/100
        Ftab_l = stats.f.ppf(1 - alpha/2, n - 1, (n - 1)*(k - 1))
        Ftab_u = stats.f.ppf(1 - alpha/2, (n - 1)*(k - 1), n - 1)
        FL = Fobs/Ftab_l
        FU = Fobs*Ftab_u
        lbound = (FL - 1)/(FL + (k - 1))
        ubound = (FU - 1)/(FU + (k - 1))
        cis = [lbound, ubound]
    elif icc_type == 'rc':
        # Repeatablity coefficient
        # NOT AN ICC, BUT INCLUDED HERE AS SAME CALCULATIONS ARE USED
        # See Barnhart & Barboriak, Trans Onc, 2009;2:231–235
        alpha = 1 - conf_level/100
        s_w = np.sqrt(MSw)
        rc = stats.norm.ppf(1-alpha/2)*np.sqrt(2)*s_w
        icc = rc
        
        # CIs
        num = n*(k-1)*(s_w**2)

        denom_l = stats.chi2.ppf(1-alpha/2, n*(k-1))
        lbound = stats.norm.ppf(1-alpha/2)*np.sqrt(2) * np.sqrt(num/denom_l)
        
        denom_u = stats.chi2.ppf(alpha/2, n*(k-1))
        ubound = stats.norm.ppf(1-alpha/2)*np.sqrt(2) * np.sqrt(num/denom_u)

        cis = [lbound, ubound]
        
        # Other outputs not used here
        p_value = None
        F_value = None
    elif icc_type == 'wcv':
        # Within-subject coefficient of variation
        # NOT AN ICC, BUT INCLUDED HERE AS SAME CALCULATIONS ARE USED
        # See Barnhart & Barboriak, Trans Onc, 2009;2:231–235
        alpha = 1 - conf_level/100
        s_w = np.sqrt(MSw)
        wcv = s_w/np.mean(repeat_measurements.flatten())
        icc = wcv
        
        # Other outputs not used here
        cis = [icc, icc]
        p_value = None
        F_value = None
    else:
        print("!!! Only ICC_1_1, ICC_2_1, ICC_2_1_c, RC, and wCV implemented so far")
    
    return icc, cis, p_value, F_value