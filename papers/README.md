# Summary of papers relevant to the project

For each new paper, please:

1. Add an Bibtex style bibliography entry to papers.bib

2. Add a brief description (ideally with a URL to the full-text) of the paper to the relevant section below

Please do NOT add pdfs of the full-text to this folder.

## Papers

### Radiomics in MRI
+ McHugh *et al*, Image Contrast, Image Pre-Processing, and T1 Mapping Affect MRI Radiomic Feature Repeatability in Patients with Colorectal Cancer Liver Metastases. Damien's original repeatability paper [full text](https://www.mdpi.com/2072-6694/13/2/240/htm)

### General radiomics

### Statistics

### Power calculations for predictive models


