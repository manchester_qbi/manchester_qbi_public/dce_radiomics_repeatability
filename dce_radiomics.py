#----------------------------------------------------------
import os
import numpy as np

import dce_radiomics_stats
import dce_radiomics_plots
import pickle

rand_seed = 42
n_boots = 1000

#----------------------------------------------------------
# Run the actual radiomics measures on the original DCE param maps. 
# This may take a long time to process but only needs to be done once
# For each dataset, we do this once each for normalised/original 
study_dirs = {
    'travastin': 'X:\\projects2\\travastin',
    'bevacizumab': 'X:\\data\\bevacizumab'
}

#----------------------------------------------------------
def compute_radiomics(study_name, map_names = ['Ktrans', 'IAUC60', 'v_e', 'v_p'], norms=['false']):
    '''
    '''
    with open(os.path.join('data', f'{study_name}_patients.pkl'), 'rb') as input:
        patients = pickle.load(input)

    visits = {
        'travastin': [1, 2, 3, 4],
        'bevacizumab': [1, 2, 4, 6]
    }

    for norm in norms:
        
        data_file = f'data/{study_name}_normalize_{norm}.pkl'
        try:
            with open(data_file, 'rb') as input:
                rad_data = pickle.load(input)
            print(f'Loaded radiomics data from {data_file}')
        except IOError:
            rad_data = []
            print('Created new data structure')

        rad_params_file = f'Params_normalise_{norm}.yaml'
        dce_radiomics_stats.compute_radiomics_features(
            study_dirs[study_name], 
            patients, 
            visits[study_name],
            map_names, 
            rad_params_file, 
            rad_data
        );

        with open(data_file, 'wb') as output:
            pickle.dump(rad_data, output, pickle.HIGHEST_PROTOCOL)

#----------------------------------------------------------
def count_tumours(study_name):
    '''
    '''
    with open(os.path.join('data', f'{study_name}_patients.pkl'), 'rb') as input:
        patients = pickle.load(input)

    rad_data = load_rad_data(study_name, 'false')

    xls_path = os.path.join('data', 
        f'{study_name}_tumour_counts.xlsx')
    dce_radiomics_stats.tumour_counts_to_excel(
        patients, rad_data, xls_path)

#----------------------------------------------------------
def features(features_list_file = 'feature_names.txt'):
    '''Return list of features'''
    with open(features_list_file) as file:
        rad_features = [line.rstrip() for line in file.readlines()]
    return rad_features

#----------------------------------------------------------
def load_rad_data(study_name, norm='false', base_dir = 'data'):
    if study_name == 'combined':
        data_file = os.path.join(base_dir, f'travastin_normalize_{norm}.pkl')
        with open(data_file, 'rb') as input:
            rad_data = pickle.load(input)

        data_file = os.path.join(base_dir, f'bevacizumab_normalize_{norm}.pkl')
        with open(data_file, 'rb') as input:
            rad_data += pickle.load(input)

    else:
        data_file = os.path.join(base_dir, f'{study_name}_normalize_{norm}.pkl')
        with open(data_file, 'rb') as input:
            rad_data = pickle.load(input)

    return rad_data

def compute_bootstrap_idx(n_data, n_boots, rand_seed = 42):
    np.random.seed(rand_seed)
    return np.random.randint(0, n_data, size = (n_data, n_boots))

#----------------------------------------------------------
# Compute summary statistics spreadsheet
def make_summary_xls(
    study_name, boxcox = False, map_names = ['Ktrans', 'IAUC60', 'v_e', 'v_p'], tumours='all_tumours'):
    rad_data = load_rad_data(study_name, 'false')
    xls_path = os.path.join('results', tumours, study_name, 'summary_stats')
    if boxcox:
        xls_path += '_bc'
    dce_radiomics_stats.features_info_to_csv(
        rad_data, boxcox, map_names, tumours, xls_path)
    
#----------------------------------------------------------
# Compute summary statistics spreadsheet
def make_tumour_counts_table(study_name):
    rad_data = load_rad_data(study_name, 'false')
    csv_path = os.path.join('results', study_name+'_tumour_counts.csv')

    dce_radiomics_stats.tumour_counts_to_csv(rad_data, csv_path)

#----------------------------------------------------------
# Compute ICC values
# We do this for normalised/original and in each for original and Box-Cox transformed
def compute_icc(study_name, boxcox=True, tumours='all_tumours', n_boots=0):
    '''
    '''
    rad_data = load_rad_data(study_name, 'false')

    if n_boots:
        rad_tumour_fun = dce_radiomics_stats.select_tumour_fun(tumours)
        feat_values = rad_tumour_fun(rad_data, 'Ktrans', 'original_firstorder_Median', [1])
        bootstrap_idx = compute_bootstrap_idx(feat_values.shape[0], n_boots, rand_seed)
        boot_str = '_boot'
    else:
        boot_str = ''

    if n_boots:
        icc_data = dce_radiomics_stats.compute_baseline_icc_data_bootstrap(
            rad_data, boxcox, tumours, bootstrap_idx)
    else:
        icc_data = dce_radiomics_stats.compute_baseline_icc_data(
            rad_data, boxcox, tumours)

    if boxcox:
        bc_str = '_bc'
    else:
        bc_str = ''

    icc_file = os.path.join('data', tumours, 
        f'{study_name}_icc{bc_str}{boot_str}.pkl')
    os.makedirs(os.path.dirname(icc_file), exist_ok=True)

    with open(icc_file, 'wb') as output:
        pickle.dump(icc_data, output, pickle.HIGHEST_PROTOCOL)

#----------------------------------------------------------
# Compute ICC values
# We do this for normalised/original and in each for original and Box-Cox transformed
def compute_wCV(study_name, boxcox=True, tumours='all_tumours', n_boots=0):
    '''
    '''
    rad_data = load_rad_data(study_name, 'false')

    if n_boots:
        rad_tumour_fun = dce_radiomics_stats.select_tumour_fun(tumours)
        feat_values = rad_tumour_fun(rad_data, 'Ktrans', 'original_firstorder_Median', [1])
        bootstrap_idx = compute_bootstrap_idx(feat_values.shape[0], n_boots, rand_seed)
        boot_str = '_boot'
    else:
        boot_str = ''

    if n_boots:
        pass
    else:
        wcv_data = dce_radiomics_stats.compute_baseline_wCV_data(
            rad_data, boxcox, tumours)

    if boxcox:
        bc_str = '_bc'
    else:
        bc_str = ''

    wcv_file = os.path.join('data', tumours, 
        f'{study_name}_wcv{bc_str}{boot_str}.pkl')
    os.makedirs(os.path.dirname(wcv_file), exist_ok=True)

    with open(wcv_file, 'wb') as output:
        pickle.dump(wcv_data, output, pickle.HIGHEST_PROTOCOL)

#----------------------------------------------------------
# Compute treatment z-score values
# We do this for normalised/original
def compute_treatment_effect(study_name, boxcox = True, 
    tumours='all_tumours', treatment_visit=3, n_boots=0):
    '''
    '''
    rad_data = load_rad_data(study_name, 'false')

    if n_boots:
        rad_tumour_fun = dce_radiomics_stats.select_tumour_fun(tumours)
        feat_values = rad_tumour_fun(rad_data, 'Ktrans', 'original_firstorder_Median', [1])
        bootstrap_idx = compute_bootstrap_idx(feat_values.shape[0], n_boots, rand_seed)
        boot_str = '_boot'
        treatment_effect_data = dce_radiomics_stats.compute_treatment_effect_data_bootstrap(
            rad_data, treatment_visit, tumours, boxcox, bootstrap_idx)
    else:
        boot_str = ''
        treatment_effect_data = dce_radiomics_stats.compute_treatment_effect_data(
            rad_data, treatment_visit, tumours, boxcox)

    if boxcox:
        bc_str = '_bc'
    else:
        bc_str = ''

    treatment_effect_file = os.path.join('data', tumours, 
        f'{study_name}_treatment_effect{bc_str}{treatment_visit}{boot_str}.pkl')

    with open(treatment_effect_file, 'wb') as output:
        pickle.dump(treatment_effect_data, output, pickle.HIGHEST_PROTOCOL)

#----------------------------------------------------------
# Count how many patients changed at a treatment visit
def compute_treatment_tumours(study_name, boxcox = True, 
    tumours='all_tumours', treatment_visit=3, n_boots=0):
    '''
    '''
    rad_data = load_rad_data(study_name, 'false')

    if n_boots:
        rad_tumour_fun = dce_radiomics_stats.select_tumour_fun(tumours)
        feat_values = rad_tumour_fun(rad_data, 'Ktrans', 'original_firstorder_Median', [1])
        bootstrap_idx = compute_bootstrap_idx(feat_values.shape[0], n_boots, rand_seed)
        boot_str = '_boot'
        treatment_tumours_data = dce_radiomics_stats.compute_treatment_tumours_data_bootstrap(
            rad_data, treatment_visit, tumours, boxcox, bootstrap_idx)
    else:
        boot_str = ''
        treatment_tumours_data = dce_radiomics_stats.compute_treatment_tumours_data(
            rad_data, treatment_visit, tumours, boxcox)

    if boxcox:
        bc_str = '_bc'
    else:
        bc_str = ''

    treatment_tumours_file = os.path.join('data', tumours, 
        f'{study_name}_treatment_tumours{bc_str}{treatment_visit}{boot_str}.pkl')

    with open(treatment_tumours_file, 'wb') as output:
        pickle.dump(treatment_tumours_data, output, pickle.HIGHEST_PROTOCOL)

#----------------------------------------------------------
# Compute correlations between features at each visit
# We do this for normalised/original and in each for original and Box-Cox transformed
def compute_features_correlation(study_name, 
    boxcox = True, tumours='all_tumours', visit = 1, n_boots=0):
    '''
    '''
    rad_data = load_rad_data(study_name, 'false')

    if n_boots:
        rad_tumour_fun = dce_radiomics_stats.select_tumour_fun(tumours)
        feat_values = rad_tumour_fun(rad_data, 'Ktrans', 'original_firstorder_Median', [1])
        bootstrap_idx = compute_bootstrap_idx(feat_values.shape[0], n_boots, rand_seed)
        boot_str = '_boot'
        corr_data = dce_radiomics_stats.compute_feature_correlation_data_bootstrap(
            rad_data, visit, tumours, boxcox, bootstrap_idx)
    else:
        boot_str = ''
        corr_data = dce_radiomics_stats.compute_feature_correlation_data(
            rad_data, visit, tumours, boxcox)

    if boxcox:
        bc_str = '_bc'
    else:
        bc_str = ''

    corr_file = os.path.join('data', tumours, 
        f'{study_name}_corr{bc_str}{visit}{boot_str}.pkl')

    with open(corr_file, 'wb') as output:
        pickle.dump(corr_data, output, pickle.HIGHEST_PROTOCOL)

    print(f'saved correlation data to {corr_file}')

#----------------------------------------------------------
def compute_measure_bootstrap_mean(measure_data, 
    map_names = ['Ktrans', 'IAUC60', 'v_e', 'v_p']):
    ''' '''
    measure_data_mean = dict()
    for map_name in map_names:
        map_measure_data = measure_data[map_name]
        map_measure_data_mean = dict() 

        for feature in map_measure_data.keys():
            feature_measure = map_measure_data[feature]
            feature_measure_mean = dict()
            if feature_measure is not None:
                for key, values in feature_measure.items():
                    feature_measure_mean[key] = np.mean(values)
        
            map_measure_data_mean[feature] = feature_measure_mean
        measure_data_mean[map_name] = map_measure_data_mean

    return measure_data_mean

def make_boot_means(study_name, bcs=[True, False], tumours='all_tumours'):

    for bc in bcs:

        if bc:
            bc_str = '_bc'
            title_info = f'box-cox transformed'
        else:
            bc_str = ''
            title_info = f'original'

        icc_name = f'{study_name}_icc{bc_str}'
        icc_file = os.path.join('data', tumours, icc_name + '_boot.pkl')

        with open(icc_file, 'rb') as input:
            icc_data = pickle.load(input)

        icc_data_mean = compute_measure_bootstrap_mean(icc_data)
        icc_file = os.path.join('data', tumours, icc_name + '_boot_mean.pkl')
        with open(icc_file, 'wb') as output:
            pickle.dump(icc_data_mean, output, pickle.HIGHEST_PROTOCOL)

    for visit in [3, 4]:
        treatment_effect_name = f'{study_name}_treatment_effect{bc_str}{visit}'
        treatment_effect_file = os.path.join(
            'data', tumours, treatment_effect_name + '_boot.pkl')

        with open(treatment_effect_file, 'rb') as input:
            treatment_effect_data = pickle.load(input)

        treatment_effect_data_mean = compute_measure_bootstrap_mean(
            treatment_effect_data
        )
        treatment_effect_file = os.path.join(
            'data', tumours, treatment_effect_name + '_boot_mean.pkl')
        with open(treatment_effect_file, 'wb') as output:
            pickle.dump(treatment_effect_data_mean, output, pickle.HIGHEST_PROTOCOL)

    for visit in [1, 3, 4]:
        corr_file = os.path.join('data', tumours, 
            f'{study_name}_corr{bc_str}{visit}_boot.pkl')

        with open(corr_file, 'rb') as input:
            corr_data = pickle.load(input)

        corr_data_mean = dict()
        for key, values in corr_data.items():
            corr_data_mean[key] = np.mean(values, axis=2)

        corr_file = os.path.join('data', tumours, 
            f'{study_name}_corr{visit}_boot_mean.pkl')

        with open(corr_file, 'wb') as output:
            pickle.dump(corr_data_mean, output, pickle.HIGHEST_PROTOCOL)
            
#----------------------------------------------------------
def make_icc_plot(study_name, 
    map_names = ['Ktrans', 'IAUC60', 'v_e', 'v_p'], boxcox=True, 
    tumours='all_tumours', boot_str = '', save_plot=True):
    '''
    '''
    fig_dir = os.path.join('results', tumours, study_name, 'baseline_icc')
    os.makedirs(fig_dir, exist_ok=True)

    if boxcox:
        bc_str = '_bc'
    else:
        bc_str = ''

    icc_name = f'{study_name}_icc{bc_str}{boot_str}'
    icc_file = os.path.join('data', tumours, icc_name  + '.pkl')

    with open(icc_file, 'rb') as input:
        icc_data = pickle.load(input)

    for map_name in map_names:
        if save_plot:
            fig_name = f'{icc_name}_{map_name}.png'
            fig_file = os.path.join(fig_dir, fig_name)
        else:
            fig_file = None

        dce_radiomics_plots.plot_icc_data(
            icc_data, map_name, 
            fig_file)

#----------------------------------------------------------
def make_wCV_plot(study_name, 
    map_names = ['Ktrans', 'IAUC60', 'v_e', 'v_p'], boxcox=True, 
    tumours='all_tumours', boot_str = '', save_plot=True):
    '''
    '''
    fig_dir = os.path.join('results', tumours, study_name, 'baseline_wcv')
    os.makedirs(fig_dir, exist_ok=True)

    if boxcox:
        bc_str = '_bc'
    else:
        bc_str = ''

    wcv_name = f'{study_name}_wcv{bc_str}{boot_str}'
    wcv_file = os.path.join('data', tumours, wcv_name  + '.pkl')

    with open(wcv_file, 'rb') as input:
        wcv_data = pickle.load(input)

    for map_name in map_names:
        if save_plot:
            fig_name = f'{wcv_name}_{map_name}.png'
            fig_file = os.path.join(fig_dir, fig_name)
        else:
            fig_file = None

        dce_radiomics_plots.plot_wCV_data(
            wcv_data, map_name, 
            fig_file)

#----------------------------------------------------------
def make_treatment_effect_plot(study_name, map_names = ['Ktrans', 'IAUC60', 'v_e', 'v_p'], 
    treatment_visit = 3, treatment_label = '48 hours', boxcox = True, tumours='all_tumours', 
    boot_str = '', save_plot=True,
    feature_type = 'median'):
    '''
    '''
    if boxcox:
        bc_str = '_bc'
    else:
        bc_str = ''

    fig_dir = os.path.join('results', tumours, study_name, 'treatment_effects', 
        f'visit{treatment_visit}')
    os.makedirs(fig_dir, exist_ok=True)

    treatment_effect_name = f'{study_name}_treatment_effect{bc_str}{treatment_visit}{boot_str}'
    treatment_effect_file = os.path.join('data', tumours, treatment_effect_name + '.pkl')

    with open(treatment_effect_file, 'rb') as input:
        treatment_effect_data = pickle.load(input)

    for map_name in map_names:
        if save_plot:
            fig_name = f'{treatment_effect_name}_{map_name}_{feature_type}.png'
            fig_file = os.path.join(fig_dir, fig_name)
        else:
            fig_file = None

        dce_radiomics_plots.plot_treatment_effect_data(
            treatment_effect_data, map_name, 
            treatment_label,
            fig_file, feature_type = feature_type)

#----------------------------------------------------------
def make_treatment_tumours_plot(study_name, map_names = ['Ktrans', 'IAUC60', 'v_e', 'v_p'], 
    treatment_visit = 3, treatment_label = '48 hours', boxcox = True, tumours='all_tumours', 
    boot_str = '', save_plot=True,
    feature_type = 'median'):
    '''
    '''
    if boxcox:
        bc_str = '_bc'
    else:
        bc_str = ''

    fig_dir = os.path.join('results', tumours, study_name, 'treatment_tumours', 
        f'visit{treatment_visit}')
    os.makedirs(fig_dir, exist_ok=True)

    treatment_tumours_name = f'{study_name}_treatment_tumours{bc_str}{treatment_visit}{boot_str}'
    treatment_tumours_file = os.path.join('data', tumours, treatment_tumours_name + '.pkl')

    with open(treatment_tumours_file, 'rb') as input:
        treatment_tumours_data = pickle.load(input)

    for map_name in map_names:
        if save_plot:
            fig_name = f'{treatment_tumours_name}_{map_name}_{feature_type}.png'
            fig_file = os.path.join(fig_dir, fig_name)
        else:
            fig_file = None

        dce_radiomics_plots.plot_treatment_tumours_data(
            treatment_tumours_data, map_name, 
            treatment_label,
            fig_file,
            feature_type = feature_type)

#----------------------------------------------------------
def make_treatment_overlap_plot(study_name, map_names = ['Ktrans', 'IAUC60', 'v_e', 'v_p'], 
    treatment_visit = 3, treatment_label = '48 hours', boxcox = True, tumours='all_tumours', 
    boot_str = '', save_plot=True):
    '''
    '''
    if boxcox:
        bc_str = '_bc'
    else:
        bc_str = ''

    fig_dir = os.path.join('results', tumours, study_name, 'treatment_overlap_pts', 
        f'visit{treatment_visit}')
    os.makedirs(fig_dir, exist_ok=True)

    treatment_tumours_name = f'{study_name}_treatment_tumours{bc_str}{treatment_visit}{boot_str}'
    treatment_overlap_name = f'{study_name}_treatment_overlap{bc_str}{treatment_visit}{boot_str}'
    treatment_tumours_file = os.path.join('data', tumours, treatment_tumours_name + '.pkl')

    with open(treatment_tumours_file, 'rb') as input:
        treatment_tumours_data = pickle.load(input)

    title_info = f'{treatment_label} post-treatment'

    for map_name in map_names:
        if save_plot:
            fig_name = f'{treatment_overlap_name}_{map_name}.png'
            fig_file = os.path.join(fig_dir, fig_name)
        else:
            fig_file = None

        dce_radiomics_plots.plot_treatment_change_overlap_data(
            treatment_tumours_data, map_name, 
            title_info,
            fig_file)

#----------------------------------------------------------
def make_treatment_features_plot(study_name, map_names = ['Ktrans', 'IAUC60', 'v_e', 'v_p'], 
    treatment_visit = 3, treatment_label = '48 hours', boxcox = True, tumours='all_tumours', 
    boot_str = '', save_plot=True):
    '''
    '''
    if boxcox:
        bc_str = '_bc'
        bc_dir = 'boxcox'
    else:
        bc_str = ''
        bc_dir = 'orig'

    rad_data = load_rad_data(study_name)
    treatment_tumours_name = f'{study_name}_treatment_tumours{bc_str}{treatment_visit}{boot_str}'
    treatment_tumours_file = os.path.join('data', tumours, treatment_tumours_name + '.pkl')

    with open(treatment_tumours_file, 'rb') as input:
        treatment_tumours_data = pickle.load(input)

    title_info = f'{treatment_label}'

    for map_name in map_names:
        if save_plot:
            fig_dir = os.path.join(
                'results', tumours, study_name, 'treatment_tumours', 
            f'visit{treatment_visit}', map_name, bc_dir)
            os.makedirs(fig_dir, exist_ok=True)
            save_path = os.path.join(fig_dir, treatment_tumours_name)
        else:
            save_path = None

        dce_radiomics_plots.plot_treatment_change_overlap_feature(
            rad_data, treatment_tumours_data, map_name, 
            treatment_visit, tumours, boxcox, title_info, save_path)

#----------------------------------------------------------
def make_correlation_plot(study_name, map_names = ['Ktrans', 'IAUC60', 'v_e', 'v_p'], visit = 1, 
    treatment_label = '48 hours', tumours='all_tumours', 
    boxcox = True, boot_str = '', save_plot=True):
    '''
    '''
    if boxcox:
        bc_str = '_bc'
    else:
        bc_str = ''

    fig_dir = os.path.join('results', tumours, study_name, 'visit_corr', 
        f'visit{visit}')
    os.makedirs(fig_dir, exist_ok=True)

    treatment_effect_name = f'{study_name}_treatment_effect{bc_str}3{boot_str}'
    treatment_effect_file = os.path.join('data', tumours, treatment_effect_name + '.pkl')

    with open(treatment_effect_file, 'rb') as input:
        treatment_effect_data = pickle.load(input)

    features = treatment_effect_data[map_names[0]].keys()

    corr_name = f'{study_name}_corr{bc_str}{visit}{boot_str}'
    corr_file = os.path.join('data', tumours, corr_name + '.pkl')

    with open(corr_file, 'rb') as input:
        corr_data = pickle.load(input)

    title_info = treatment_label

    for map_name in map_names:
        if save_plot:
            fig_name = f'{corr_name}_{map_name}.png'
            fig_file = os.path.join(fig_dir, fig_name)
        else:
            fig_file = None

        dce_radiomics_plots.plot_corr_data(
            corr_data, features, map_name, 
            title_info,
            fig_file)

#----------------------------------------------------------
def make_median_correlation_plot(study_name, map_names = ['Ktrans', 'IAUC60', 'v_e', 'v_p'], 
    visit = 1, treatment_label = '48 hours', tumours='all_tumours', 
    boxcox = True, boot_str = '', save_plot=True,
    feature_type = 'median'):
    '''
    '''
    if boxcox:
        bc_str = '_bc'
    else:
        bc_str = ''

    fig_dir = os.path.join('results', tumours, study_name, 'visit_corr', 
        f'visit{visit}')
    os.makedirs(fig_dir, exist_ok=True)

    treatment_effect_name = f'{study_name}_treatment_effect{bc_str}3{boot_str}'
    treatment_effect_file = os.path.join('data', tumours, treatment_effect_name + '.pkl')

    with open(treatment_effect_file, 'rb') as input:
        treatment_effect_data = pickle.load(input)

    features = treatment_effect_data[map_names[0]].keys()

    corr_name = f'{study_name}_corr{bc_str}{visit}{boot_str}'
    corr_file = os.path.join('data', tumours, corr_name + '.pkl')

    with open(corr_file, 'rb') as input:
        corr_data = pickle.load(input)

    title_info = treatment_label

    for map_name in map_names:
        if save_plot:
            fig_name = f'{corr_name}_{map_name}_{feature_type}.png'
            fig_file = os.path.join(fig_dir, fig_name)
        else:
            fig_file = None

        dce_radiomics_plots.plot_median_correlation(
            corr_data, features, map_name, 
            title_info,
            fig_file,
            feature_type = feature_type)
#----------------------------------------------------------
def make_checklist_map(study_name, 
    map_names = ['Ktrans', 'IAUC60', 'v_e', 'v_p'], tumours='all_tumours',
    boxcox = True, boot_str = '', save_plot=True):
    ''' '''
    if boxcox:
        bc_str = '_bc'
    else:
        bc_str = ''

    fig_dir = os.path.join('results', tumours, study_name, 'checklist_map')
    os.makedirs(fig_dir, exist_ok=True)

    icc_name = f'{study_name}_icc_bc{bc_str}{boot_str}'
    icc_file = os.path.join('data', tumours, icc_name + '.pkl')
    with open(icc_file, 'rb') as input:
        icc_data = pickle.load(input)

    treatment_effect_data = []
    corr_data = []

    for visit in [3,4]:
        treatment_effect_name = f'{study_name}_treatment_effect{bc_str}{visit}{boot_str}'
        treatment_effect_file = os.path.join('data', tumours, treatment_effect_name + '.pkl')

        with open(treatment_effect_file, 'rb') as input:
            treatment_effect_data.append(pickle.load(input))

        corr_name = f'{study_name}_corr{bc_str}{visit}{boot_str}'
        corr_file = os.path.join('data', tumours, corr_name + '.pkl')

        with open(corr_file, 'rb') as input:
            corr_data.append(pickle.load(input))

    for map_name in map_names:
        if save_plot:
            fig_name = f'checklist_{map_name}{boot_str}.png'
            fig_file = os.path.join(fig_dir, fig_name)
        else:
            fig_file = None

        dce_radiomics_plots.make_checklist_map(
            icc_data, treatment_effect_data, corr_data, 
            map_name, fig_file)

#----------------------------------------------------------
def make_ranking_map(study_name, 
    map_names = ['Ktrans', 'IAUC60', 'v_e', 'v_p'], tumours='all_tumours',
    boxcox = True, boot_str = '', save_plot=True):
    ''' '''
    if boxcox:
        bc_str = '_bc'
    else:
        bc_str = ''

    fig_dir = os.path.join('results', tumours, study_name, 'ranking_map')
    os.makedirs(fig_dir, exist_ok=True)

    icc_name = f'{study_name}_icc_bc{boot_str}'
    icc_file = os.path.join('data', tumours, icc_name + '.pkl')
    with open(icc_file, 'rb') as input:
        icc_data = pickle.load(input)

    treatment_effect_data = []
    treatment_tumours_data = []
    corr_data = []

    for visit in [3,4]:
        #
        treatment_effect_name = f'{study_name}_treatment_effect{bc_str}{visit}{boot_str}'
        treatment_effect_file = os.path.join('data', tumours, treatment_effect_name + '.pkl')

        with open(treatment_effect_file, 'rb') as input:
            treatment_effect_data.append(pickle.load(input))

        #
        treatment_tumours_name = f'{study_name}_treatment_tumours{bc_str}{visit}{boot_str}'
        treatment_tumours_file = os.path.join('data', tumours, treatment_tumours_name + '.pkl')

        with open(treatment_tumours_file, 'rb') as input:
            treatment_tumours_data.append(pickle.load(input))

        #
        corr_name = f'{study_name}_corr{bc_str}{visit}{boot_str}'
        corr_file = os.path.join('data', tumours, corr_name + '.pkl')

        with open(corr_file, 'rb') as input:
            corr_data.append(pickle.load(input))


    for map_name in map_names:
        if save_plot:
            fig_name = f'ranking_{map_name}{bc_str}{boot_str}.png'
            fig_file = os.path.join(fig_dir, fig_name)
        else:
            fig_file = None

        dce_radiomics_plots.make_ranking_map(
            icc_data, treatment_effect_data,treatment_tumours_data, corr_data, 
            map_name, fig_file)

#------------------------------------------------------------------
def make_features_lookup_table():
    '''
    '''
    treatment_effect_name = f'combined_treatment_effect_bc3'
    treatment_effect_file = os.path.join('data', 'all_tumours', 
        treatment_effect_name + '.pkl')

    with open(treatment_effect_file, 'rb') as input:
        treatment_effect_data = pickle.load(input)

    features = treatment_effect_data['Ktrans'].keys()

    features_lookup_file = os.path.join('data', 'features_lookup.csv')
    labels = dce_radiomics_plots.get_feature_labels(features)[0]
    with open(features_lookup_file, 'wt') as lookup_file:
        for i_f,f in enumerate(features):
            color = dce_radiomics_plots.feature_color(f).split(':')[1]
            parts = f.split('_')
            print(f'{parts[1]}, {labels[i_f]}: {parts[2]}, {color}', 
                file = lookup_file)

#------------------------------------------------------------------
def make_median_summary_plots(map_names = ['Ktrans', 'IAUC60'], 
    units = ['$min^{-1}$', '$mmol min$'], apply_boxcox = True, 
    feature_type = 'median'):

    rad_data = [load_rad_data(study_name, 'false') 
        for study_name in ['travastin', 'bevacizumab']]

    if apply_boxcox:
        bc_str = '_bc'
        units = [f'BC[{u}]' for u in units]
    else:
        bc_str = ''

    save_path = os.path.join('results', 'all_tumours', f'{feature_type}_summary')
    os.makedirs(save_path, exist_ok=True)
    for map_name, unit in zip(map_names, units):
        dce_radiomics_plots.make_median_stats_plot(
            rad_data, map_name, unit, apply_boxcox = apply_boxcox,
            save_path= os.path.join(save_path, f'{map_name}{bc_str}'), 
            feature_type = feature_type)

#------------------------------------------------------------------
def make_waterfall_plot(study_name = 'combined', map_names = ['Ktrans', 'IAUC60'],
    apply_boxcox = True, feature_type = 'median'):
    rad_data = load_rad_data(study_name, 'false')

    if apply_boxcox:
        bc_str = '_bc'
    else:
        bc_str = ''

    if feature_type == 'median':
        feature = 'original_firstorder_Median'
    else:
        feature = 'original_shape_MeshVolume'

    save_path = os.path.join('results', 'all_tumours', f'{feature_type}_waterfall')
    os.makedirs(save_path, exist_ok=True)

    for map_name in map_names:
        dce_radiomics_plots.make_median_waterfall_plot(
            rad_data, map_name, apply_boxcox = apply_boxcox,
            save_path= os.path.join(save_path, f'{map_name}_waterfall{bc_str}'),
            feature = feature)

#----------------------------------------------------------
def make_boxcox_features_plot(
    study_name, map_names = ['Ktrans', 'IAUC60', 'v_e', 'v_p'], 
    tumours='all_tumours', save_plot=True):
    '''
    '''
    rad_data = load_rad_data(study_name)

    for map_name in map_names:
        if save_plot:
            fig_dir = os.path.join(
                'results', tumours, study_name, 'boxcox_plots', map_name)
            os.makedirs(fig_dir, exist_ok=True)
            save_path = os.path.join(fig_dir, 'bc_')
        else:
            save_path = None

        rad_features = features()
        dce_radiomics_plots.plot_boxcox_figs_feature(
            rad_data, map_name, tumours, rad_features, save_path=save_path)

#------------------------------------------------------------
def make_tumour_maps(study_name, map_names = ['Ktrans', 'IAUC60', 'v_e', 'v_p']):

    visits = {
        'travastin': [1, 2, 3, 4],
        'bevacizumab': [1, 2, 4, 6]
    }

    dce_radiomics_plots.save_colorbar(
        'inferno', (256,20), os.path.join('results', 'tumour_maps'))

    with open(os.path.join('data', f'{study_name}_patients.pkl'), 'rb') as input:
        patients = pickle.load(input)

    for patient in patients:
        id = patient.id_
        patient_dir = os.path.join(study_dirs, id)
        fig_dir = os.path.join('results', 'tumour_maps', study, id)

        if patient.tumours_ is None:
            continue

        for tumour_num in range(1, len(patient.tumours_[0])+1):
            for map_name in map_names:
                try:
                    make_map_plots.make_visits_plot(
                        patient_dir, tumour_num, visits[study], 
                        map_name, fig_dir
                    )
                except:
                    pass