

import os
import numpy as np
import pandas as pd
from scipy import stats
from radiomics import featureextractor
from analyse_radiomics_functions import icc

#------------------------------------------------------------------
def get_tumour_list(patients):
    '''Return list of patient ID, tumour num tuples'''
    tumour_list = []

    for patient in patients:
        for i_tum in range(1,patient.num_tumours()+1):
            tumour_list.append((patient.id_, i_tum))

    return tumour_list

#------------------------------------------------------------------
def get_dce_dir(study_dir, tumour):
    '''
    Get directory path containing tumour DCE data, carefully navigating
    QBI's weird folder structure
    Return None if folder doesn't exist
    '''
    if tumour is None:
        return None

    patient_id = tumour.parameters_['Patient']
    visit = tumour.parameters_['Visit']
    tumour_num = tumour.parameters_['Tumour']

    dce_dir = os.path.join(
        study_dir, patient_id, f'visit{visit}', 'mdm_analysis', f'ETM_tumour{tumour_num}')
    
    if not os.path.exists(dce_dir):
        print(f'Missing data for {dce_dir}')
        return None

    return dce_dir

#------------------------------------------------------------------
def compute_tumour_features(
    study_dir, tumour, map_names, extractor, map_features):
    '''
    Compute radiomics features for a single tumour

    'Patient': 'A001_JS',
    'Visit': 2,
    'Date': datetime.datetime(2009, 11, 13, 0, 0),
    'Ktrans': 0.075401,
    've': 0.329717,
    'vp': 0.003084,
    'Observations': 'travastin1_A001_JS_v2_t1',
    'Study': 'travastin1',
    'Time': datetime.time(11, 30, 35),
    'Tumour': 1,
    'QC_Level': 4,
    'DCE_Source': 'popnAIF',
    'IAUC': 8.337379,
    'T1': 1353.1799729364,
    'WTV': 28776.586111,
    'ETV': 27677.718861,
    'EF': 96.1813842484257,
    'ADC': 0.00147,
    'ADC_2': 'NaN',
    'Injection_image': 11,
    'Onset_to_Peak': 5,
    'AIF_slice': 10
    '''
    dce_dir = get_dce_dir(study_dir, tumour)
    if dce_dir is None:
        return None

    # Set up mask path
    msk_path = os.path.join(dce_dir,'enhVox.nii.gz')
    
    #Set up image path
    for map_name in map_names:
        if map_name in map_features.keys() and not map_features[map_name] is None:
            #print(f'Skipped {map_name} using ROI {msk_path}')
            continue

        map_path = os.path.join(dce_dir, map_name + '_radiomics.nii.gz')
        
        # Calculate features
        try: 
            radiomics_dict = extractor.execute(map_path, msk_path)
    
            # ordered dict -> dict
            map_features[map_name] = dict(radiomics_dict)
            print(f'Computed features for {map_name} using ROI {msk_path}')

        except:
            print(f'Error computing features for {map_name} using ROI {msk_path}')
            map_features[map_name] = None
    

#------------------------------------------------------------------
def compute_radiomics_features(
    study_dir, patients, visits, map_names, rad_params_file, rad_data=[]):
    '''
    Compute radiomics values for all tumours at selected
    creating data a structure that is:
    -> list of patient data
      -> list of tumour data
        -> list of visit data
          -> dict of map names (Ktrans, IAUC etc)
            -> dict of radiomics features
    '''
    make_new = not rad_data

    #Set up feature extractor
    # Set up pyradiomics and output directory  
    extractor = featureextractor.RadiomicsFeatureExtractor(
        rad_params_file)

    #Loop over patients data info
    for i_pt,patient in enumerate(patients):

        print(f'Computing features for patient {i_pt} of {len(patients)}')
        
        #If we're making rad_data from scratch add a new empty list
        if make_new:
            rad_data.append([])

        patient_data = rad_data[i_pt]

        for i_tum in range(patient.num_tumours()):
            if len(patient_data) <= i_tum:
                patient_data.append([])

            tumour_data = patient_data[i_tum]

            for i_visit,visit in enumerate(visits):

                if len(tumour_data) <= i_visit:
                    tumour_data.append(dict())

                visit_data = tumour_data[i_visit]
                compute_tumour_features(
                        study_dir,
                        patient.tumours_[visit-1][i_tum],
                        map_names,
                        extractor,
                        visit_data
                        )
    
    return rad_data

#
def apply_bootstrap(rad_vals, bootstrap_idx):
    rad_vals_array = np.array(rad_vals)
    if bootstrap_idx is not None:
        rad_vals_array = rad_vals_array[bootstrap_idx,:]
    return rad_vals_array

#-----------------------------------------------------------------
def tumour_counts_to_excel(patients, rad_data, xls_path):
    n_pts = len(patients)
    n_visits = 4

    #Create empty array for tumour counts and list for IDs
    tumour_counts = np.full((n_pts+1, n_visits), np.NaN)
    patient_ids = list()
    for i_pt, patient in enumerate(patients):
        for i_v in range(n_visits):
            tumour_counts[i_pt, i_v] = sum([bool(t[i_v]) for t in rad_data[i_pt]])

        patient_ids.append(patient.id_)

    #Sum the counts and append to final row
    patient_ids.append('Total')
    tumour_counts[-1,:] = np.sum(tumour_counts[:-1,:], axis = 0)

    #Create dataframe for XLS file
    df_id = pd.DataFrame(data=patient_ids, 
        index=patient_ids, columns=['Patient ID'])
    visit_cols = ['Baseline 1', 'Baseline 2', '48hrs', 'EC1']
    df_data = pd.DataFrame(
        data=tumour_counts, index=patient_ids, columns=visit_cols)
    df = pd.concat([df_id, df_data], axis=1)

    #Write dataframe to XLS file
    with pd.ExcelWriter(xls_path) as writer:
        df.to_excel(writer, index=False)

#------------------------------------------------------------------
def rad_tumour1(rad_data, map_name, rad_feature, visits, bootstrap_idx=None):
    '''
    Return np vector for a single radiomics feature for the first
    tumour of each patient at a given set of visits
    '''
    n_pts = len(rad_data)
    n_visits = len(visits)

    #Create empty array for rad vals
    rad_vals = np.full((n_pts, n_visits), np.NaN)

    #Loop through patients
    for i_pt, pt_data in enumerate(rad_data):
        if not pt_data:
            continue
        
        for i_visit, visit in enumerate(visits):

            for tumour_data in pt_data:
                valid = (len(tumour_data) > visit-1) and \
                        (tumour_data[visit-1] is not None) and \
                            map_name in tumour_data[visit-1].keys()

                if valid:
                    break

            if valid:
                rad_vals[i_pt,i_visit] = \
                    tumour_data[visit-1][map_name][rad_feature]

    return apply_bootstrap(rad_vals, bootstrap_idx)

#------------------------------------------------------------------
def rad_tumour_avg(rad_data, map_name, rad_feature, visits, bootstrap_idx=None):
    '''
    Return np vector for a single radiomics feature as the average
    of the tumours for each patient at a given set of visits
    '''
    n_pts = len(rad_data)
    n_visits = len(visits)

    #Create empty array for rad vals
    rad_vals = np.full((n_pts, n_visits), np.NaN)

    #Loop through patients
    for i_pt, pt_data in enumerate(rad_data):
        for i_visit, visit in enumerate(visits):
            feat_sum = 0
            n_tumours = 0
            for tumour in pt_data:
                try:
                    feat_sum += tumour[visit-1][map_name][rad_feature]
                    n_tumours += 1
                except:
                    pass
            if n_tumours:
                rad_vals[i_pt,i_visit] = feat_sum / n_tumours

    return apply_bootstrap(rad_vals, bootstrap_idx)

#------------------------------------------------------------------
def rad_tumour_all(rad_data, map_name, rad_feature, visits, bootstrap_idx=None):
    '''
    Return np vector for a single radiomics feature for all
    tumour of each patient at a given visit
    '''
    #Init rad vals as a list we can then convert to array
    rad_vals = []
    n_visits = len(visits)

    #Loop through patients
    for i_pt, pt_data in enumerate(rad_data):
        for tumour in pt_data:   
            visit_vals = np.full((n_visits), np.NaN)         
            for i_visit, visit in enumerate(visits):  

                try:
                    visit_vals[i_visit] = tumour[visit-1][map_name][rad_feature]                   
                except:
                    pass

            rad_vals.append(visit_vals)

    return apply_bootstrap(rad_vals, bootstrap_idx)
#-----------------------------------------------------------------
def select_tumour_fun(tumours):
    if tumours == 'all_tumours':
        rad_tumour_fun = rad_tumour_all
    elif tumours == 'single_tumour':
        rad_tumour_fun = rad_tumour1
    elif tumours == 'avg_tumours':
        rad_tumour_fun = rad_tumour_avg
    else:
        raise RuntimeError(f'Incorrect tumours options {tumours}')
    return rad_tumour_fun

#-----------------------------------------------------------------
def rad_data_to_excel(patients, rad_data, map_names, visits, features, xls_path):
    ''' Save out radiomics data to XLS file'''

    #Get tumour list and make dataframes from ID and tumour number
    tumour_list = get_tumour_list(patients)
    df_id = pd.DataFrame(data=[t[0] for t in tumour_list], 
            index=tumour_list, columns=['Patient ID'])
    df_tn = pd.DataFrame(data=[t[1] for t in tumour_list], 
            index=tumour_list, columns=['Tumour'])

    with pd.ExcelWriter(xls_path) as writer:
        for map_name in map_names:
            #Set up list of dataframes to join, starting with ID + tumour number
            frames = [df_id, df_tn]

            for feature in features:
                v12 = rad_tumour_all(rad_data, map_name, feature, visits)
                visit_cols = [f'{feature}: visit {v}' for v in visits]
                frames.append(pd.DataFrame(data=v12, index=tumour_list, columns=visit_cols))

            #Join the frames horizontally
            df = pd.concat(frames,axis=1)
            df.to_excel(writer, sheet_name=map_name, index=False)


#------------------------------------------------------------------
def boxcox_lambda(feature_data):
    '''
    Apply Box-cox transform to combined visit 1 and 2 data
    '''
    #Get min and max and shift data
    valid = np.isfinite(feature_data)
    f_min = feature_data[valid].min()

    feature_data += (1e-6 - f_min)

    v_12 = feature_data[:,:].flatten()#0:2
    v_12_valid = v_12[np.isfinite(v_12)]
            
    # Find lambda giving 'most-normal' transformed distribution
    lmbda = stats.boxcox_normmax(v_12_valid, brack = (-1.9, 2.0),
                                         method = 'mle')

    return lmbda

#------------------------------------------------------------------
def boxcox_transform(feature_data, lmbda):
    '''
    Apply Box-cox transform to combined visit 1 and 2 data
    '''
    valid = np.isfinite(feature_data)
    try:
        feature_data[valid] = stats.boxcox(feature_data[valid], lmbda)
        #feature_data[valid] = np.log(feature_data[valid])
    except ValueError:
        print('Box-cox transform failed. Setting all data to zeros')
        feature_data[:] = 0

    if not np.any(np.isfinite(feature_data)):
        print('All data non-finite. Setting all data to zeros')
        feature_data[:] = 0

#------------------------------------------------------------------
def boxcox_inverse(feature_data, lmbda, f_min):
    '''
    Apply Box-cox transform to combined visit 1 and 2 data
    Box-cox does y = log(x) if lmbda = 0 and y = (x**lmbda - 1) / lmbda
    otherwise
    '''
    y = feature_data
    if lmbda:
        x = (lmbda*y + 1)**(1 / lmbda)
    else:
        x = np.exp(y)

    x += (f_min - 1e-6)
    return x


#------------------------------------------------------------------
def compute_baseline_icc(rad_data, map_name, rad_feature, apply_boxcox, tumours, bootstrap_idx = None):
    '''
    Compute ICC values for a given feature of a given map type
    '''
    rad_tumour_fun = select_tumour_fun(tumours)
    v1234_data = rad_tumour_fun(rad_data, map_name, rad_feature, [1,2,3,4], bootstrap_idx)
    v12_data = v1234_data[:,0:2]

    if apply_boxcox:
        lmbda = boxcox_lambda(v1234_data)
        boxcox_transform(v12_data, lmbda)

    valid = np.all(np.isfinite(v12_data), axis=1)

    icc_feat = icc(v12_data[valid,:], 'icc_1_1')
    icc_dict = {
        'icc':icc_feat[0], 
        'CI_lo':icc_feat[1][0], 
        'CI_hi':icc_feat[1][1], 
        'p_value':icc_feat[2], 
        'F_value':icc_feat[3]}
    return icc_dict

#------------------------------------------------------------------
def compute_baseline_icc_data(rad_data, apply_boxcox, tumours):
    '''
    Compute ICC data for all features and map types
    '''
    icc_data = dict()
    for patient in rad_data:
        for tumour in patient:
            for visit in tumour:
                map_names = visit.keys()
                if map_names:
                    break
            if map_names:
                break
        if map_names:
            break

    for map_name in map_names:
        map_icc_data = dict()

        features = [key for key in visit[map_name].keys() 
            if key.startswith('original')]

        for feature in features:

            try:
                map_icc_data[feature] = compute_baseline_icc(
                    rad_data, map_name, feature, apply_boxcox, tumours)
            except:
                print(f'failed for {map_name}, {feature}')
                map_icc_data[feature] = None

        icc_data[map_name] = map_icc_data

    return icc_data

#------------------------------------------------------------------
def compute_baseline_icc_data_bootstrap(rad_data, apply_boxcox, tumours, bootstrap_idx):
    '''
    Compute ICC data for all features and map types
    '''
    icc_data = dict()
    for patient in rad_data:
        for tumour in patient:
            for visit in tumour:
                map_names = visit.keys()
                if map_names:
                    break
            if map_names:
                break
        if map_names:
            break

    n_boots = bootstrap_idx.shape[1]

    for map_name in map_names:
        map_icc_data = dict()

        features = [key for key in visit[map_name].keys() 
            if key.startswith('original')]

        for feature in features:
            map_icc_data[feature] = dict()

            print(f'Computing ICC for boostraps for feature {feature}')
                
            for i_boot in range(n_boots):
                idx = bootstrap_idx[:,i_boot]

                map_icc_data_feature = compute_baseline_icc(
                        rad_data, map_name, feature, apply_boxcox, tumours, idx)

                #Copy the values from the ICC data structure into
                #a new dictionary with the same keys, but the values stored
                #as array over the bootstrap indices
                for key,value in map_icc_data_feature.items():
                    if not i_boot: #init array on first boot
                        map_icc_data[feature][key] = np.full(n_boots, np.NaN)
                        
                    map_icc_data[feature][key][i_boot] = value

        icc_data[map_name] = map_icc_data

    return icc_data

#------------------------------------------------------------------
def compute_baseline_wCV(rad_data, map_name, rad_feature, apply_boxcox, tumours, bootstrap_idx = None):
    '''
    Compute wCV values for a given feature of a given map type
    '''
    rad_tumour_fun = select_tumour_fun(tumours)
    v1234_data = rad_tumour_fun(rad_data, map_name, rad_feature, [1,2,3,4], bootstrap_idx)
    v12_data = v1234_data[:,0:2]

    if apply_boxcox:
        lmbda = boxcox_lambda(v1234_data)
        boxcox_transform(v12_data, lmbda)

    not_finite = ~np.all(np.isfinite(v12_data), axis=1)
    v12_data[not_finite,:] = 0
    valid = np.abs(np.mean(v12_data, axis=1)) > 0

    if np.any(valid):
        v12_mean = np.mean(v12_data[valid,:], axis = 1)
        v12_diff = v12_data[valid,0] - v12_data[valid,1]

        wcv2 = np.mean( (v12_diff*v12_diff) / (2*v12_mean*v12_mean))
    else:
        wcv2 = 0

    wcv_dict = {'wcv':np.sqrt(wcv2)}
    return wcv_dict

#------------------------------------------------------------------
def compute_baseline_wCV_data(rad_data, apply_boxcox, tumours):
    '''
    Compute wCV data for all features and map types
    '''
    wcv_data = dict()
    for patient in rad_data:
        for tumour in patient:
            for visit in tumour:
                map_names = visit.keys()
                if map_names:
                    break
            if map_names:
                break
        if map_names:
            break

    for map_name in map_names:
        map_wcv_data = dict()

        features = [key for key in visit[map_name].keys() 
            if key.startswith('original')]

        for feature in features:

            try:
                map_wcv_data[feature] = compute_baseline_wCV(
                    rad_data, map_name, feature, apply_boxcox, tumours)
            except:
                print(f'failed for {map_name}, {feature}')
                map_wcv_data[feature] = None

        wcv_data[map_name] = map_wcv_data

    return wcv_data

#------------------------------------------------------------------
def compute_main_features(v_data, v_dict):
    '''
    '''
    v_dataf = v_data.flatten()
    v_dataf = v_dataf[np.isfinite(v_dataf)]

    v_dict['Mean'] = np.mean(v_dataf) 
    v_dict['Median'] = np.median(v_dataf)
    v_dict['STD'] = np.std(v_dataf, ddof = 1) 
    v_dict['IQR'] = stats.iqr(v_dataf) 
    sh = stats.shapiro(v_dataf)
    v_dict['Normal-h'] = sh[0]
    v_dict['Normal-p'] = sh[1]

#------------------------------------------------------------------
def compute_corr_features(baseline_data, diff_data, v_dict):
    '''
    '''
    good = np.isfinite(baseline_data) & np.isfinite(diff_data)
    baseline_data = baseline_data[good]
    diff_data = diff_data[good]

    sp = stats.spearmanr(baseline_data, diff_data)
    pe = stats.pearsonr(baseline_data, diff_data)
    v_dict['Spearman-r'] = sp[0] 
    v_dict['Spearman-p'] = sp[1]
    v_dict['Pearson-r'] = pe[0] 
    v_dict['Pearson-p'] = pe[1]

#------------------------------------------------------------------
def compute_equal_vars(baseline_data, v1_data, v2_data, v_dict):
    good = np.isfinite(baseline_data)
    v1_data = v1_data[good]
    v2_data = v2_data[good]

    v12_equal_vars = stats.levene(v1_data, v2_data)
    v_dict['Equal-vars-h'] = v12_equal_vars[0]
    v_dict['Equal-vars-p'] = v12_equal_vars[1]

#------------------------------------------------------------------
def feature_info_to_df(
    rad_data, map_name, visit_dfs, rad_feature, tumours, boxcox = False):
    '''
    Standard checks applied to visit data
    - Are data normally distributed
      - For visit data
      - For difference between baselines
      - For difference from baseline to visit
    - Are variances of visit 1 and visit 2 equal
    - Is there any correlation between 
        - baseline difference and baseline mean
        - visit difference to baseline mean and baseline mean
    '''

    #Get data for visits 1 to 4 for this map/radiomics feature
    rad_tumour_fun = select_tumour_fun(tumours)
    v1234_data = rad_tumour_fun(
        rad_data, map_name, rad_feature, [1,2,3,4])

    #Compute boxcox transform
    if boxcox:
        lmbda = boxcox_lambda(v1234_data)
        boxcox_transform(v1234_data, lmbda)
    
    #Do joint baseline data
    baseline_data = np.mean(v1234_data[:,0:2], axis=1)
    v_str = 'V1V2'

    #Compute main feature summary stats
    v_summary = {'Feature' : rad_feature}
    compute_main_features(baseline_data, v_summary)

    #Do the equal variances test
    compute_equal_vars(
        baseline_data, v1234_data[:,0], v1234_data[:,1], v_summary)

    #Create dataframe and append to main frame
    visit_dfs[v_str].append(v_summary)

    #Now do individual visit data 
    for visit in range(4):
        #Get visit data from main array
        v_str = f'V{visit+1}'
        v_data = v1234_data[:,visit]

        #Compute main feature summary stats
        v_summary = {'Feature' : rad_feature}
        compute_main_features(v_data, v_summary)

        #Create dataframe and append to main frame
        visit_dfs[v_str].append(v_summary)

        if visit != 1:
            #For visit 1 do diff between baselines
            if visit == 0:
                diff_data = v_data - v1234_data[:,1]
                v_str = 'dV1V2'
            else:
                #For visits 3 and 4 do diff to baseline
                diff_data = v_data - baseline_data
                v_str = f'dV{visit+1}'

            #Compute main feature summary stats
            v_summary = {'Feature' : rad_feature}
            compute_main_features(diff_data, v_summary)
            compute_corr_features(baseline_data, diff_data, v_summary)

            #Create dataframe and append to main frame
            visit_dfs[v_str].append(v_summary)
    
#------------------------------------------------------------------
def count_p_vals(sheet_df):
    '''
    For any column containing p-values (name ends '-p'), count how many
    are < 0.05
    '''
    row = 0
    for col in sheet_df.columns:
        if col[-2:] == '-p':
            count = (sheet_df[col] > 0.05).sum()
            if not row:
                row = sheet_df[col].size
            sheet_df.loc[row, col] = count



#------------------------------------------------------------------
def features_info_to_csv(rad_data, boxcox, map_names, tumours, xls_path):
    '''
    Write summary info on each feature to csv
    '''
    sheets = ['V1V2', 'V1', 'V2', 'V3', 'V4', 'dV1V2', 'dV3', 'dV4']
    
    #Loop through each map name
    for map_name in map_names:

        #Get list of features
        features = [key for key in rad_data[1][0][0][map_name].keys() 
            if key.startswith('original')]

        #Set up an empty dict to store dataframes for this map
        visit_dfs = dict()

        #Set up new dataframe for each visit, these will be sheets in the
        #final csv
        for sheet in sheets:
            visit_dfs[sheet] = []

        #Populate sheets with summary statistics for each feature
        #1 feature per row
        for feature in features:
            feature_info_to_df(
                rad_data, map_name, visit_dfs, feature, tumours, boxcox)

        #Write the filled sheets to an XLSX file
        xls_map_path = f'{xls_path}_{map_name}.xlsx'
        with pd.ExcelWriter(xls_map_path, engine='xlsxwriter') as writer:  
            for sheet in sheets:
                sheet_df = pd.DataFrame(visit_dfs[sheet])
                count_p_vals(sheet_df)
                print(f'{xls_map_path}: {sheet}')
                #Now write this dataframe to an excel file
                sheet_df.to_excel(writer, sheet_name=sheet, index=False)

#------------------------------------------------------------------
def tumour_counts_to_csv(rad_data, csv_path):
    '''
    Write summary info on each feature to csv
    '''
    #Get data for visits 1 to 4 for this map/radiomics feature
    subject_counts = tumour_counts_by_visit(rad_data, 'avg_tumours')
    tumour_counts = tumour_counts_by_visit(rad_data, 'all_tumours')

    rows = ['yyyy','yyyn','yyny','yynn','ynyy','ynyn','ynny','ynnn']
    os.makedirs(os.path.dirname(csv_path), exist_ok=True)
    with open(csv_path, 'wt') as f:
        for row in rows:
            print(f'{row}, {subject_counts[row]}, {tumour_counts[row]}',
                  file = f)

#------------------------------------------------------------------
def tumour_counts_by_visit(rad_data, tumours):
    rad_tumour_fun = select_tumour_fun(tumours)
    v1234_data = rad_tumour_fun(
        rad_data, 'Ktrans', 'original_firstorder_Median', [1,2,3,4])

    v1234_exists = np.isfinite(v1234_data)
    b1 = np.sum(v1234_exists[:,:2], axis = 1) == 1
    b2 = np.sum(v1234_exists[:,:2], axis = 1) == 2
    v34_exists = v1234_exists[:,2:]

    counts = dict()

    counts['yyyy'] = np.sum(b2 & np.all(v34_exists, axis=1))
    counts['yyyn'] = np.sum(b2 & v34_exists[:,0] & ~v34_exists[:,1])
    counts['yyny'] = np.sum(b2 & ~v34_exists[:,0] & v34_exists[:,1])
    counts['yynn'] = np.sum(b2 & ~np.any(v34_exists, axis=1))
    
    counts['ynyy'] = np.sum(b1 & np.all(v34_exists, axis=1))
    counts['ynyn'] = np.sum(b1 & v34_exists[:,0] & ~v34_exists[:,1])
    counts['ynny'] = np.sum(b1 & ~v34_exists[:,0] & v34_exists[:,1])
    counts['ynnn'] = np.sum(b1 & ~np.any(v34_exists, axis=1))
    return counts


#------------------------------------------------------------------
def compute_baseline_repeatability(rad_data, map_name, rad_feature, apply_boxcox, tumours, bootstrap_idx = None):
    '''
    Compute ICC values for a given feature of a given map type
    '''
    rad_tumour_fun = select_tumour_fun(tumours)
    v1234_data = rad_tumour_fun(rad_data, map_name, rad_feature, [1,2,3,4], bootstrap_idx)
    v12_data = v1234_data[:,0:2]

    if apply_boxcox:
        lmbda = boxcox_lambda(v1234_data)
        boxcox_transform(v12_data, lmbda)

    valid = np.all(np.isfinite(v12_data), axis=1)
 
    std = np.std(v12_data[valid,0]-v12_data[valid,1])
    coeff_repeatability = 1.96*std
    
    return coeff_repeatability

#------------------------------------------------------------------
def compute_baseline_repeatability_data(rad_data, apply_boxcox, tumours):
    '''
    Compute ICC data for all features and map types
    '''
    repeatability_data = dict()
    for patient in rad_data:
        for tumour in patient:
            for visit in tumour:
                map_names = visit.keys()
                if map_names:
                    break
            if map_names:
                break
        if map_names:
            break

    for map_name in map_names:
        map_repeatability_data = dict()

        features = [key for key in visit[map_name].keys() 
            if key.startswith('original')]

        for feature in features:
            try:
                map_repeatability_data[feature] = compute_baseline_repeatability(
                    rad_data, map_name, feature, apply_boxcox, tumours)
            except:
                print(f'failed for {map_name}, {feature}')
                map_repeatability_data[feature] = None

        repeatability_data[map_name] = map_repeatability_data

    return repeatability_data

#------------------------------------------------------------------
def compute_treatment_effect(
    rad_data, map_name, rad_feature, treatment_visit, tumours, apply_boxcox, bootstrap_idx = None):
    '''
    Compute ICC values for a given feature of a given map type
    '''
    rad_tumour_fun = select_tumour_fun(tumours)
    v1234_data = rad_tumour_fun(rad_data, map_name, rad_feature, [1,2,3,4], bootstrap_idx)
    v12 = v1234_data[:,0:2]
    v_treatment = v1234_data[:,treatment_visit-1]

    #Box-cox transform it if set
    if apply_boxcox:
        lmbda = boxcox_lambda(v1234_data)
        boxcox_transform(v12, lmbda)
        boxcox_transform(v_treatment, lmbda)

    valid_baseline = np.all(np.isfinite(v12), axis=1)
    valid_treatment = np.isfinite(v_treatment)
    valid = valid_baseline & valid_treatment

    if np.sum(valid) < 10:
        return None

    v12_good = v12[valid,:]
    v_treatment = v_treatment[valid]

    v12_diff = v12_good[:,0]-v12_good[:,1]
    v12_diff_mean = np.mean(v12_diff)
    v12_diff_std = np.sqrt(np.sum(v12_diff**2)/(2*len(v12_diff)))

    v12_avg = np.mean(v12_good, axis=1)

    v_treatment_diff = v_treatment - v12_avg
    v_treatment_diff_z = (v_treatment_diff) / v12_diff_std

    n_valid = v12_good.shape[0]
    treatment_effect = dict()
    treatment_effect['mean'] = np.mean(v_treatment_diff_z)
    treatment_effect['std'] = np.std(v_treatment_diff_z)
    treatment_effect['se'] = treatment_effect['std'] / np.sqrt(n_valid)
    treatment_effect['diff'] = v12_diff
    treatment_effect['diff_mean'] = v12_diff_mean
    treatment_effect['diff_std'] = v12_diff_std
    treatment_effect['diff_shapiro'] = stats.shapiro(v12_diff)
    return treatment_effect

#------------------------------------------------------------------
def compute_treatment_effect_data(rad_data, treatment_visit, tumours, apply_boxcox):
    '''
    Compute treatment for all features and map types
    '''
    treatment_effect_data = dict()
    map_names = rad_data[1][0][0].keys()

    for map_name in map_names:
        map_treatment_effect_data = dict()
        features = [key for key in rad_data[1][0][0][map_name].keys() 
            if key.startswith('original')]

        for feature in features:
            map_treatment_effect_data[feature] = compute_treatment_effect(
                rad_data, map_name, feature, treatment_visit, tumours, apply_boxcox)

        treatment_effect_data[map_name] = map_treatment_effect_data

    return treatment_effect_data

#------------------------------------------------------------------
def compute_treatment_effect_data_bootstrap(rad_data, treatment_visit, tumours, apply_boxcox, bootstrap_idx):
    '''
    Compute treatment for all features and map types
    '''
    #Set up main data structure as empty dictionary
    treatment_effect_data = dict()
    map_names = rad_data[1][0][0].keys()

    n_boots = bootstrap_idx.shape[1]

    #Loop over all map types and all features
    for map_name in map_names:
        map_treatment_effect_data = dict()
        features = [key for key in rad_data[1][0][0][map_name].keys() 
            if key.startswith('original')]

        for feature in features:
            print(f'Computing TE for boostraps for feature {feature}')
            map_treatment_effect_data[feature] = dict()

            #For bootstrapping, get the set of sample indices for each boot
            #then compute treatment_effect for resampled rad_data
            for i_boot in range(n_boots):
                idx = bootstrap_idx[:,i_boot]

                map_treatment_effect_data_feature = compute_treatment_effect(
                    rad_data, map_name, feature, treatment_visit, tumours, apply_boxcox, idx)

                #Copy the values from the treatment_effect data structure into
                #a new dictionary with the same keys, but the values stored
                #as array over the bootstrap indices
                for key,value in map_treatment_effect_data_feature.items():
                    if key in ['diff', 'diff_shapiro']:
                        continue
                    if not i_boot: #init array on first boot
                        map_treatment_effect_data[feature][key] = np.full(n_boots, np.NaN)
                        
                    map_treatment_effect_data[feature][key][i_boot] = value

        #Save the final map data into the main data structure
        treatment_effect_data[map_name] = map_treatment_effect_data

    return treatment_effect_data

#------------------------------------------------------------------
def get_treatment_delta(
    rad_data, map_name, rad_feature, treatment_visit, tumours, 
    apply_boxcox, bootstrap_idx = None):
    '''
    '''
    #Get data for this feature and map
    rad_tumour_fun = select_tumour_fun(tumours)
    v1234_data = rad_tumour_fun(rad_data, map_name, rad_feature, [1,2,3,4], bootstrap_idx)
    v12 = v1234_data[:,0:2]
    v_treatment = v1234_data[:,treatment_visit-1]

    #Box-cox transform it if set
    if apply_boxcox:
        lmbda = boxcox_lambda(v1234_data)
        boxcox_transform(v12, lmbda)
        boxcox_transform(v_treatment, lmbda)

    #Get idx of valid finite values
    valid_baseline = np.all(np.isfinite(v12), axis=1)
    valid_treatment = np.isfinite(v_treatment)
    valid = valid_baseline & valid_treatment

    return v12, v_treatment, valid, valid_baseline, valid_treatment

#------------------------------------------------------------------
def compute_RC(baseline_diff):
    N = len(baseline_diff)
    wVar = np.sum(baseline_diff**2) / (2*N)
    wSD = np.sqrt(wVar)
    RC = 1.96*np.sqrt(2)*wSD
    return RC

#------------------------------------------------------------------
def compute_treatment_tumours(
    rad_data, map_name, rad_feature, treatment_visit, tumours, 
    apply_boxcox, bootstrap_idx = None):
    '''
    Count number of patients with significant change
    '''
    #Get data for this feature and map
    v12, v_treatment, valid, valid_baseline, _ = get_treatment_delta(
        rad_data, map_name, rad_feature, treatment_visit, tumours, 
        apply_boxcox, bootstrap_idx)

    if np.sum(valid) < 10:
        return None

    #Compute baseline difference and std of difference
    baseline_diff = v12[valid_baseline,0] - v12[valid_baseline,1]

    #Repeatability co-efficient is 1.96 x baseline diff std
    RC = compute_RC(baseline_diff)
    if rad_feature == 'original_firstorder_Median':
        print(f'{map_name} RC = {RC}')

    #Now compute difference of treatment visit to mean baseline and count how many
    #patients have absolute difference greater than RC
    v_diff = v_treatment[valid] - np.mean(v12[valid,:], axis=1)

    change_patients = np.full_like(v_treatment, False)
    change_patients[valid] = np.abs(v_diff) > RC
    change_patients_list = np.nonzero(change_patients)
    num_change_patients = np.sum(change_patients)

    change_pts = dict()
    change_pts['RC'] = RC
    change_pts['change_patients_list'] = change_patients_list
    change_pts['num_change_patients'] = num_change_patients
    return change_pts

#------------------------------------------------------------------
def compute_treatment_tumours_data(rad_data, treatment_visit, tumours, apply_boxcox):
    '''
    Compute treatment for all features and map types
    '''
    treatment_tumours_data = dict()
    map_names = rad_data[1][0][0].keys()

    for map_name in map_names:
        map_treatment_tumours_data = dict()
        features = [key for key in rad_data[1][0][0][map_name].keys() 
            if key.startswith('original')]

        for feature in features:
            map_treatment_tumours_data[feature] = compute_treatment_tumours(
                rad_data, map_name, feature, treatment_visit, tumours, apply_boxcox)

        treatment_tumours_data[map_name] = map_treatment_tumours_data

    return treatment_tumours_data

#------------------------------------------------------------------
def compute_treatment_tumours_data_bootstrap(rad_data, treatment_visit, tumours, apply_boxcox, bootstrap_idx):
    '''
    Compute treatment for all features and map types
    '''
    #Set up main data structure as empty dictionary
    treatment_tumours_data = dict()
    map_names = rad_data[1][0][0].keys()

    n_boots = bootstrap_idx.shape[1]

    #Loop over all map types and all features
    for map_name in map_names:
        map_treatment_tumours_data = dict()
        features = [key for key in rad_data[1][0][0][map_name].keys() 
            if key.startswith('original')]

        for feature in features:
            print(f'Computing TE for boostraps for feature {feature}')
            map_treatment_tumours_data[feature] = dict()

            #For bootstrapping, get the set of sample indices for each boot
            #then compute treatment_tumours for resampled rad_data
            for i_boot in range(n_boots):
                idx = bootstrap_idx[:,i_boot]

                map_treatment_tumours_data_feature = compute_treatment_tumours(
                    rad_data, map_name, feature, treatment_visit, tumours, apply_boxcox, idx)

                #Copy the values from the treatment_tumours data structure into
                #a new dictionary with the same keys, but the values stored
                #as array over the bootstrap indices
                for key,value in map_treatment_tumours_data_feature.items():
                    if key in ['change_patients_list']:
                        continue
                    if not i_boot: #init array on first boot
                        map_treatment_tumours_data[feature][key] = np.full(n_boots, np.NaN)
                        
                    map_treatment_tumours_data[feature][key][i_boot] = value

        #Save the final map data into the main data structure
        treatment_tumours_data[map_name] = map_treatment_tumours_data

    return treatment_tumours_data

#------------------------------------------------------------------
def compute_feature_correlation(
    rad_data, map_name, features, visit, tumours, apply_boxcox, bootstrap_idx = None):
    '''
    Compute ICC values for a given feature of a given map type
    '''
    n_features = len(features)
    n_samples = 0

    #For each feature, get visit data
    for i_f,feature in enumerate(features):
        rad_tumour_fun = select_tumour_fun(tumours)
        v1234_data = rad_tumour_fun(rad_data, map_name, feature, [1,2,3,4], bootstrap_idx)
        
        if apply_boxcox:
            lmbda = boxcox_lambda(v1234_data)
            boxcox_transform(v1234_data, lmbda)

        v12 = np.nanmean(v1234_data[:,0:2], axis=1)

        if visit == 1:
            v_data = v12
        else:
            v_data = v1234_data[:,visit-1] - v12

        #If features matrix not initialised, init now
        if not n_samples:
            n_samples = v_data.shape[0]
            features_matrix = np.empty((n_samples,n_features))
        
        #Save feature data as column in matrix
        features_matrix[:,i_f] = v_data

        if feature == 'original_firstorder_Median':
            med_idx = i_f
        elif feature == 'original_shape_MeshVolume':
            vol_idx = i_f

    #Remove NaNs 
    valid = np.all(np.isfinite(features_matrix), axis=1)

    #Compute correlation coefficients matrix
    features_matrix = features_matrix[valid,:]
    features_corr = np.corrcoef(features_matrix, rowvar=False)

    #Compute significance to median and tumour volume
    features_median_r = np.zeros(n_features)
    features_median_p = np.zeros(n_features)
    features_volume_r = np.zeros(n_features)
    features_volume_p = np.zeros(n_features)

    med_data = features_matrix[:,med_idx]
    vol_data = features_matrix[:,vol_idx]
    for i_f in range(n_features):
        [r,p] = stats.pearsonr(med_data, features_matrix[:,i_f])
        features_median_r[i_f] = r
        features_median_p[i_f] = p

        [r,p] = stats.pearsonr(vol_data, features_matrix[:,i_f])
        features_volume_r[i_f] = r
        features_volume_p[i_f] = p

    return features_corr, features_median_r, features_median_p, features_volume_r, features_volume_p

#------------------------------------------------------------------
def compute_feature_correlation_data(rad_data, visit, tumours, apply_boxcox):
    '''
    Compute ICC data for all features and map types
    '''
    corr_data = dict()
    map_names = rad_data[1][0][0].keys()

    for map_name in map_names:
        features = [key for key in rad_data[1][0][0][map_name].keys() 
            if key.startswith('original')]

        map_features_cov = compute_feature_correlation(
            rad_data, map_name, features, visit, tumours, apply_boxcox)

        corr_data[map_name] = dict()
        corr_data[map_name]['corr_matrix'] = map_features_cov[0]
        corr_data[map_name]['median_r'] = map_features_cov[1]
        corr_data[map_name]['median_p'] = map_features_cov[2]
        corr_data[map_name]['volume_r'] = map_features_cov[3]
        corr_data[map_name]['volume_p'] = map_features_cov[4]

    return corr_data

#------------------------------------------------------------------
def compute_feature_correlation_data_bootstrap(
    rad_data, visit, tumours, apply_boxcox, bootstrap_idx):

    n_boots = bootstrap_idx.shape[1]

    corr_data = dict()
    map_names = rad_data[1][0][0].keys()

    for map_name in map_names:
        features = [key for key in rad_data[1][0][0][map_name].keys() 
            if key.startswith('original')]

        #For bootstrapping, get the set of sample indices for each boot
        #then compute treatment_effect for resampled rad_data
        for i_boot in range(n_boots):
            idx = bootstrap_idx[:,i_boot]

            map_features_cov = compute_feature_correlation(
                rad_data, map_name, features, visit, tumours, apply_boxcox, idx)

            #For first shape make a container for the correlation matrix of each bootstrap
            if not i_boot:
                f_shape = list(map_features_cov.shape) + [n_boots]
                corr_data[map_name] = np.zeros(f_shape)

            corr_data[map_name][:,:,i_boot] = map_features_cov

    return corr_data