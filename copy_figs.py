'''
'''
#%%
%load_ext autoreload

%autoreload 2

%config Completer.use_jedi = False

#%%
import os
import shutil
import skimage.transform as skt
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
#%%
fig_dir = 'C://Users//momeemb2//Dropbox (The University of Manchester)//paper_submissions//dce_radiomics_2022//figs//final_figs'
results_dir = os.path.join('../results')

#%%
def create_letter(text, width):
    fig = plt.figure()
    fig.tight_layout(pad=0)
    mpl.rcParams['text.antialiased']=False
    plt.text(0, 0, f'{text})', fontweight = 'bold', fontsize = 64)
    plt.axis('off')

    # If we haven't already shown or saved the plot, then we need to
    # draw the figure first...
    fig.canvas.draw()

    # Now we can save it to a numpy array.
    data = np.frombuffer(fig.canvas.tostring_rgb(), dtype=np.uint8)
    data = data.reshape(fig.canvas.get_width_height()[::-1] + (3,))
    rows,cols,_ = np.nonzero(~data)
    sr = np.min(rows)
    er = np.max(rows+1)
    sc = np.min(cols)
    ec = np.max(cols+1)
    data = data[sr:er, sc:ec]

    scale = width / data.shape[1]
    height = np.round(scale*data.shape[0])
    letter = skt.resize(data, (height,width))
    alpha = np.ones(letter.shape[:-1] + (1,))#np.atleast_3d(np.all(letter < np.max(letter), axis = 2))
    letter = np.concatenate((letter,alpha), axis = 2)

    return letter

def add_letter(img, text, width, pos):
    letter = create_letter(text, width)

    lh, lw, _ = letter.shape
    img[pos[0]:pos[0] + lh, pos[1]:pos[1] + lw, :] = letter

#%%
'''
    Figure 1 is copied from the hi-res slide export of the features
    look-up table in dce_radiomics_May22_paper_figures.pptx
'''    
def make_fig_1():
    #Simply need to copy the source image from the results dir to the figure dir
    src_pth = os.path.join(fig_dir, '..', '..', 
        'dce_radiomics_May22_paper_figures.png')
    dst_pth = os.path.join(fig_dir, 'fig1.png')
    shutil.copyfile(src_pth, dst_pth)

'''
    Figure 2 needs composing from 4 different sub-images:

    a) Maps of Ktrans at all 4 visits for Travastin pt A012, tumour 1, 
        made using make_map_plots.make_visits_plot, produing png:
        results/tumour_maps/travastin/A012_RH/Ktrans_t1_v_all.png
    b) A Bland-Altman plot of median Ktrans at the two baseline visits for all tumours, 
        made using travastin_radiomics.make_median_stats_plot, produing png:
        results/all_tumours/median_summary/Ktrans_bc_baseline_bland.png 
    c) A line plot of the cohort level change in median Ktrans from baseline to each post-treatment visit,
        made using travastin_radiomics.make_median_stats_plot, produing png: 
        results/all_tumours/median_summary/Ktrans_bc_treatment_lineplot.png
    d) A waterfall plot of individual tumours change in median Ktrans from baseline,
        made using travastin_radiomics.make_median_waterfall_plot, producing png: 
        results/all_tumours/median_waterfall/Ktrans_waterfall_bc.png

        The sub-images need combining in the form:

        -----------------------
        |          a          |
        -----------------------
        |    b     |     c    |
        -----------------------
        |          d          |
        |                     |
        -----------------------

        Note (d) consists of two plots stacked vertically. In the figure legend these are separated
        so the final figure has 5 parts, a - e.
'''
def make_fig_2():
    #Read in the 4 figure parts
    #part_a = plt.imread(os.path.join(results_dir, 'tumour_maps', 'travastin', 
    #    'A012_RH', 'Ktrans_t1_v_all.png'))
    part_a = plt.imread(os.path.join(results_dir, 'tumour_maps',
        'A012_Ktrans_t1_v_all.png'))
    part_b = plt.imread(os.path.join(results_dir, 'all_tumours', f'median_summary', 
        'Ktrans_bc_baseline_bland.png'))
    part_c = plt.imread(os.path.join(results_dir, 'all_tumours', f'median_summary', 
        'Ktrans_bc_treatment_lineplot.png'))
    part_d = plt.imread(os.path.join(results_dir, 'all_tumours', f'median_waterfall', 
        'Ktrans_waterfall_bc.png'))

    #Resize as necessary
    h_a, w_a ,_ = part_a.shape
    h_b, w_b ,_ = part_b.shape
    h_c, w_c ,_ = part_c.shape
    h_d, w_d ,_ = part_d.shape

    #Width of image is set by part a
    w_im = w_a

    #Resize part_c first to have the same height as part_b, and
    #then so that the combined width of b and c is the same a part a
    scale_c = h_b / h_c
    scale_bc = w_im / (w_b + np.round(scale_c*w_c))

    h_b2 = np.round(scale_bc*h_b)
    w_b2 = np.round(scale_bc*w_b)
    part_b = skt.resize(part_b, (h_b2,w_b2))

    w_c2 = w_im - w_b2
    part_c = skt.resize(part_c, (h_b2,w_c2))

    #Resize part d so its width is the same as part a
    scale_d = w_im / w_d
    h_d2 = np.round(scale_d*h_d)
    part_d = skt.resize(part_d, (h_d2,w_im))

    #Add letters to the parts
    add_letter(part_a, 'a', 32, (0,0))
    add_letter(part_b, 'b', 32, (0,0))
    add_letter(part_c, 'c', 32, (0,0))
    add_letter(part_d, 'd', 32, (0,0))
    add_letter(part_d, 'e', 32, (int(part_d.shape[0]/2),0))

    #Stack the parts together
    part_bc = np.concatenate((part_b,part_c), axis=1)
    final_fig = np.concatenate((part_a, part_bc, part_d), axis=0)

    #Save the output figure
    dst_pth = os.path.join(fig_dir, f'fig2.png')
    plt.imsave(dst_pth, final_fig)

#%%
'''
    Figure 3 is composed of a single part, the bar chart of baseline ICCs,
    made using travastin_radiomics.make_icc_plot, producing a png:
    results/all_tumours/combined/baseline_icc/combined_icc_bc_Ktrans.png
'''    
def make_fig_3():
    #Simply need to copy the source image from the results dir to the figure dir
    src_pth = os.path.join(results_dir, 'all_tumours', 'combined', 
        'baseline_icc', 'combined_icc_bc_Ktrans.png')
    dst_pth = os.path.join(fig_dir, 'fig3.png')
    shutil.copyfile(src_pth, dst_pth)

'''
'''
def make_fig_45(visit):
    #Load in the parts for 48hrs and EC1
    effects_dir = os.path.join(results_dir, 'all_tumours', 'combined', 'treatment_effects')
    corr_dir = os.path.join(results_dir, 'all_tumours', 'combined', 'visit_corr')
    tumours_dir = os.path.join(results_dir, 'all_tumours', 'combined', 'treatment_tumours')
    
    
    part_a = plt.imread(os.path.join(
        effects_dir, f'visit{visit}', f'combined_treatment_effect_bc{visit}_Ktrans_median.png'))
    part_b = plt.imread(os.path.join(
        corr_dir, f'visit{visit}', f'combined_corr_bc{visit}_Ktrans_median.png'))
    part_c = plt.imread(os.path.join(
        tumours_dir, f'visit{visit}', f'combined_treatment_tumours_bc{visit}_Ktrans_median.png'))

    w_a = part_a.shape[1]
    h_b, w_b ,_ = part_b.shape
    h_c, w_c ,_ = part_c.shape

    if w_b != w_a:
        scale_b = w_b / w_a
        h_b2 = np.round(scale_b*h_b)
        part_b = skt.resize(part_b, (h_b2,w_a))

    if w_c != w_a:
        scale_c = w_c / w_a
        h_c2 = np.round(scale_c*h_c)
        part_c = skt.resize(part_c, (h_c2,w_a))
    
    add_letter(part_a, 'a', 32, (0,0))
    add_letter(part_b, 'b', 32, (0,0))
    add_letter(part_c, 'c', 32, (0,0))

    final_fig = np.concatenate((part_a, part_b, part_c), axis = 0)

    if visit == 3:
        dst_pth = os.path.join(fig_dir, 'fig4.png')
    else:
        dst_pth = os.path.join(fig_dir, 'fig5.png')
    
    plt.imsave(dst_pth, final_fig)

'''
    Figure 6 is composed of 1 part, the desired properties ranking map plot,
    made using travastin_radiomics.make_ranking_map, produing png:
    results/all_tumours/combined/ranking_map/ranking_Ktrans_bc.png
'''
def make_fig_6():
    #Simply need to copy the source image from the results dir to the figure dir
    src_pth = os.path.join(results_dir, 'all_tumours', 'combined', 
        'ranking_map', 'ranking_Ktrans_bc.png')
    dst_pth = os.path.join(fig_dir, 'fig6.png')
    shutil.copyfile(src_pth, dst_pth)

#%%
'''
'''
def make_fig_S1():
    #Read in the 4 figure parts
    part_a = plt.imread(os.path.join(results_dir, 'all_tumours', 
        'combined', 'boxcox_plots', 'Ktrans', 'bc__GLSZM_14_00.png'))
    part_b = plt.imread(os.path.join(results_dir, 'all_tumours', 
        'combined', 'boxcox_plots', 'Ktrans', 'bc__Shape_13_10.png'))
    part_c = plt.imread(os.path.join(results_dir, 'all_tumours', 
        'combined', 'boxcox_plots', 'Ktrans', 'bc__First-order_14_11.png'))

    w_a = part_a.shape[1]
    h_b, w_b ,_ = part_b.shape
    h_c, w_c ,_ = part_c.shape

    if w_b != w_a:
        scale_b = w_b / w_a
        h_b2 = np.round(scale_b*h_b)
        part_b = skt.resize(part_b, (h_b2,w_a))

    if w_c != w_a:
        scale_c = w_c / w_a
        h_c2 = np.round(scale_c*h_c)
        part_c = skt.resize(part_c, (h_c2,w_a))
        
    final_fig = np.concatenate((part_a, part_b, part_c), axis=0)

    #Save the output figure
    dst_pth = os.path.join(fig_dir, f'figS1.png')
    plt.imsave(dst_pth, final_fig)

'''
'''
def make_fig_S2():
    #Simply need to copy the source image from the results dir to the figure dir
    src_pth = os.path.join(results_dir, 'all_tumours', 'combined', 
        'baseline_wcv', 'combined_wcv_bc_Ktrans.png')
    dst_pth = os.path.join(fig_dir, 'figS2.png')
    shutil.copyfile(src_pth, dst_pth)

'''
'''
def make_fig_S3():
    features_dir = os.path.join(results_dir, 
        'all_tumours', 'combined', 'treatment_tumours', 'visit3', 'Ktrans', 'boxcox')
    part_a = plt.imread(os.path.join(features_dir, 'combined_treatment_tumours_bc3_SHAPE 2.png'))
    part_b = plt.imread(os.path.join(features_dir, 'combined_treatment_tumours_bc3_GLRLM 7.png'))
    
    n_cols_a = part_a.shape[1]
    n_cols_b = part_b.shape[1]

    if n_cols_a > n_cols_b:
        part_b = np.pad(part_b, ((0,0), (0, n_cols_a-n_cols_b), (0,0)))
    elif n_cols_b > n_cols_a:
        part_a = np.pad(part_a, ((0,0), (0, n_cols_b-n_cols_a), (0,0)))

    add_letter(part_a, 'a', 32, (0,0))
    add_letter(part_a, 'b', 32, (0,part_a.shape[1]-33))
    add_letter(part_b, 'c', 32, (0,0))
    add_letter(part_b, 'd', 32, (0,part_a.shape[1]-33))

    final_fig = np.concatenate((part_a, part_b), axis = 0)

    dst_pth = os.path.join(fig_dir, 'figS3.png')
    plt.imsave(dst_pth, final_fig)

'''
'''
def make_fig_S4():
    #Read in the 4 figure parts
    part_b = plt.imread(os.path.join(results_dir, 'all_tumours', f'volume_summary', 
        'Ktrans_bc_baseline_bland.png'))
    part_c = plt.imread(os.path.join(results_dir, 'all_tumours', f'volume_summary', 
        'Ktrans_bc_treatment_lineplot.png'))
    part_d = plt.imread(os.path.join(results_dir, 'all_tumours', f'volume_waterfall', 
        'Ktrans_waterfall_bc.png'))

    #Resize as necessary
    h_b, w_b ,_ = part_b.shape
    h_c, w_c ,_ = part_c.shape
    h_d, w_d ,_ = part_d.shape

    #Width of image is set by part a
    w_im = w_d

    #Resize part_c first to have the same height as part_b, and
    #then so that the combined width of b and c is the same a part a
    scale_c = h_b / h_c
    scale_bc = w_im / (w_b + np.round(scale_c*w_c))

    h_b2 = np.round(scale_bc*h_b)
    w_b2 = np.round(scale_bc*w_b)
    part_b = skt.resize(part_b, (h_b2,w_b2))

    w_c2 = w_im - w_b2
    part_c = skt.resize(part_c, (h_b2,w_c2))

    #Add letters to the parts
    add_letter(part_b, 'a', 32, (0,0))
    add_letter(part_c, 'b', 32, (0,0))
    add_letter(part_d, 'c', 32, (0,0))
    add_letter(part_d, 'd', 32, (int(part_d.shape[0]/2),0))

    #Stack the parts together
    part_bc = np.concatenate((part_b,part_c), axis=1)
    final_fig = np.concatenate((part_bc, part_d), axis=0)

    #Save the output figure
    dst_pth = os.path.join(fig_dir, f'figS4.png')
    plt.imsave(dst_pth, final_fig)

def make_fig_S5():
    #Load in the parts for 48hrs and EC1
    corr_dir = os.path.join(results_dir, 'all_tumours', 'combined', 'visit_corr')
    tumours_dir = os.path.join(results_dir, 'all_tumours', 'combined', 'treatment_tumours')
    
    
    part_a = plt.imread(os.path.join(
        corr_dir, f'visit3', f'combined_corr_bc3_Ktrans_volume.png'))
    part_b = plt.imread(os.path.join(
        tumours_dir, f'visit3', f'combined_treatment_tumours_bc3_Ktrans_volume.png'))
    part_c = plt.imread(os.path.join(
        corr_dir, f'visit4', f'combined_corr_bc4_Ktrans_volume.png'))
    part_d = plt.imread(os.path.join(
        tumours_dir, f'visit4', f'combined_treatment_tumours_bc4_Ktrans_volume.png'))

    w_a = part_a.shape[1]
    h_b, w_b ,_ = part_b.shape
    h_c, w_c ,_ = part_c.shape
    h_d, w_d ,_ = part_d.shape

    if w_b != w_a:
        scale_b = w_b / w_a
        h_b2 = np.round(scale_b*h_b)
        part_b = skt.resize(part_b, (h_b2,w_a))

    if w_c != w_a:
        scale_c = w_c / w_a
        h_c2 = np.round(scale_c*h_c)
        part_c = skt.resize(part_c, (h_c2,w_a))

    if w_d != w_a:
        scale_d = w_d / w_a
        h_d2 = np.round(scale_d*h_d)
        part_d = skt.resize(part_d, (h_d2,w_a))
    
    add_letter(part_a, 'a', 32, (0,0))
    add_letter(part_b, 'b', 32, (0,0))
    add_letter(part_c, 'c', 32, (0,0))
    add_letter(part_d, 'd', 32, (0,0))

    final_fig = np.concatenate((part_a, part_b, part_c, part_d), axis = 0)

    dst_pth = os.path.join(fig_dir, 'figS5.png')
    plt.imsave(dst_pth, final_fig)


# %%
#Main paper figs
make_fig_1()
make_fig_2()
make_fig_3()
for visit in [3, 4]:
    make_fig_45(visit)
make_fig_6()

#%%
#Supplementary figs
make_fig_S1()
make_fig_S2()
make_fig_S3()
make_fig_S4()
make_fig_S5()


# %%
