48h
original_firstorder_10Percentile: [ 0.77708929  1.02664525 19.         11.         -0.40177696  1.04940797
 20.         11.         -0.54742539]
EC1
original_shape_LeastAxisLength: [ 0.94238981  0.06077259  8.          8.         -0.14299034  0.43140965
 22.         18.         -0.24032637]
original_shape_MajorAxisLength: [ 0.98207183  0.24758801  4.          3.         -0.11206191  0.63756538
 17.         14.         -0.04802687]
original_shape_Maximum2DDiameterColumn: [ 0.97706399  0.28227149  9.          9.         -0.07031669  0.51237159
 15.         12.         -0.07239038]
original_shape_Maximum2DDiameterRow: [ 0.96869779  0.10839941  7.          5.         -0.07051037  0.56162198
 13.         10.         -0.02482676]
original_shape_Maximum2DDiameterSlice: [ 0.98252494  0.22148173  4.          4.         -0.11213386  0.66543782
 16.         14.         -0.09920471]
original_shape_Maximum3DDiameter: [ 0.97833017  0.25700508  8.          7.         -0.02928208  0.54453893
 16.         13.         -0.10844428]
original_shape_MeshVolume: [ 0.99187238  0.24667919 15.         13.         -0.10505257  1.43177252
 46.         37.         -0.17558711]
original_shape_MinorAxisLength: [ 0.98448568  0.25081885 12.          9.         -0.04784231  0.95194625
 26.         22.         -0.07196801]
original_shape_SurfaceArea: [ 0.9825729   0.15757637  3.          3.         -0.03673269  0.80048101
 23.         19.         -0.12817283]
original_shape_SurfaceVolumeRatio: [ 0.91011379  0.09678149  4.          4.         -0.10625557  0.58818662
 19.         15.         -0.18697523]
original_shape_VoxelVolume: [ 0.99184436  0.24531947 15.         13.         -0.10427072  1.42730869
 44.         35.         -0.17529631]
original_glrlm_RunLengthNonUniformity: [ 0.96610568  0.82813856 15.          8.         -0.67444387  1.35566776
 34.         22.         -0.5245022 ]
original_glszm_GrayLevelNonUniformity: [ 0.93323754  0.04978043  5.          5.         -0.18784288  0.48802927
 12.         10.         -0.2235209 ]
original_glszm_LargeAreaHighGrayLevelEmphasis: [ 0.86994655  0.29134742 10.          9.         -0.11296794  0.44169894
 14.         11.         -0.09351144]
